<?php

namespace Database\Factories\Master;

use App\Models\Master\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Product::class;

    public function definition()
    {
        return [
            'product_code' => $this->faker->numerify('########'),
            'product_name' => $this->faker->userName(),
            'unit_id' => 2,
            'unit' => 2,
            'category_id' => 2,
            'category' => 2,
            'merk_id' => 2,
            'merk' => 2,
            'description' => '-',
            'bkp' => 0,
            'purchase_price' => $this->faker->numerify('########'),
            'ppn_product' => 1,
            'pph_product' => 1,
            'dpp_product' => 1,
            'selling_price' => $this->faker->numerify('########'),
            'min_stock' => 10,
            'max_stock' => 100,
            'product_photo' => '-',
            'store_id' => 1,
            'status_product' => 0,
            'user_created' => 'faker',
            'user_updated' => 'faker',
        ];
    }
}
