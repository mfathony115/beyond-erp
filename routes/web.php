<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Master\CategoryController;
use App\Http\Controllers\Master\MenuController;
use App\Http\Controllers\Master\MerkController;
use App\Http\Controllers\Master\PositionsController;
use App\Http\Controllers\Master\ProductController;
use App\Http\Controllers\Master\UnitController;
use App\Http\Controllers\Master\UserController;
use App\Http\Controllers\Master\VendorController;
use App\Http\Controllers\Master\WarehouseController;
use App\Http\Controllers\Purchasing\PurchaseOrderController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth', 'verified'])->group(function () {
    Route::get('/', [DashboardController::class, 'index']);
    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('setting/vendor', [VendorController::class, 'index'])->name('setting/vendor');
    Route::middleware(['checkRole'])->group(function(){
        Route::get('setting/user', [UserController::class, 'index'])->name('setting/user');
        Route::get('setting/position', [PositionsController::class, 'index'])->name('setting/position');
        Route::get('setting/category', [CategoryController::class, 'index'])->name('setting/category');
        Route::get('setting/merk', [MerkController::class, 'index'])->name('setting/merk');
        Route::get('setting/unit', [UnitController::class, 'index'])->name('setting/unit');
        Route::get('setting/product', [ProductController::class, 'index'])->name('setting/product');
        Route::get('setting/warehouse', [WarehouseController::class, 'index'])->name('setting/warehouse');
        Route::get('setting/menu', [MenuController::class, 'index'])->name('setting/menu');
    });

    Route::get('purchase/order', [PurchaseOrderController::class, 'index'])->name('purchase/order');
    // Route::prefix('setting')->group(function(){
    //     Route::get('warehouse', [WarehouseController::class, 'index'])->name('warehouse');
    // });
    #data
    Route::get('user/data/{id}', [UserController::class, 'allData']);
    Route::post('user', [UserController::class, 'store']);
    Route::post('user/update/{id}', [UserController::class, 'update']);
    Route::get('user/{id}', [UserController::class, 'getById']);
    Route::delete('user/{id}', [UserController::class, 'delete']);

    Route::get('position/data/{id}', [PositionsController::class, 'allData']);
    Route::put('position/{id}', [PositionsController::class, 'update']);
    Route::delete('position/{id}', [PositionsController::class, 'delete']);
    Route::get('position/master/menu/{id}/{position_id}', [PositionsController::class, 'getMasterMenu']);
    Route::post('position/master/menu/addrole', [PositionsController::class, 'addRoleByPosition']);
    Route::post('position/master/menu/deleterole', [PositionsController::class, 'deleteRoleByPosition']);

    Route::post('menu/position/role', [PositionsController::class, 'roleByPosition']);
    Route::get('menu/position/sort', [PositionsController::class, 'sortRoleMenu']);
    Route::post('menu/move/byrole', [PositionsController::class, 'moveMenuByRole']);

    Route::post('category/data/{id}', [CategoryController::class, 'allData']);
    Route::post('category', [CategoryController::class, 'store']);
    Route::put('category/{id}', [CategoryController::class, 'update']);
    Route::delete('category/{id}', [CategoryController::class, 'delete']);
    
    Route::post('merk/data/{id}', [MerkController::class, 'allData']);
    Route::post('merk', [MerkController::class, 'store']);
    Route::put('merk/{id}', [MerkController::class, 'update']);
    Route::delete('merk/{id}', [MerkController::class, 'delete']);

    Route::post('unit/data/{id}', [UnitController::class, 'allData']);
    Route::post('unit', [UnitController::class, 'store']);
    Route::put('unit/{id}', [UnitController::class, 'update']);
    Route::delete('unit/{id}', [UnitController::class, 'delete']);

    Route::get('product/{id}', [ProductController::class, 'getById']);
    Route::post('product/data/{id}', [ProductController::class, 'allData']);
    Route::post('product', [ProductController::class, 'store']);
    Route::post('product/{id}', [ProductController::class, 'update']);
    Route::delete('product/{id}', [ProductController::class, 'delete']);

    Route::post('menu/data/{id}', [MenuController::class, 'allData']);
    Route::post('menu', [MenuController::class, 'store']);
    Route::put('menu/{id}', [MenuController::class, 'update']);
    Route::delete('menu/{id}', [MenuController::class, 'delete']);

    Route::post('warehouse/data/{id}', [WarehouseController::class, 'allData']);
    Route::post('warehouse', [WarehouseController::class, 'store']);
    Route::put('warehouse/{id}', [WarehouseController::class, 'update']);
    Route::delete('warehouse/{id}', [WarehouseController::class, 'delete']);    

    # Supplier
    Route::post('supplier', [VendorController::class, 'store']);
    Route::post('supplier/data/{id}', [VendorController::class, 'allData']);
    Route::put('supplier/{id}', [VendorController::class, 'update']);
    Route::delete('supplier/{id}', [VendorController::class, 'delete']);
    Route::get('supplier/search', [VendorController::class, 'searchData']);
    
});
Route::get('/user/{id}', [UserController::class, 'getById']);
Route::get('/testBase64', [ProductController::class, 'testBase64']);
Route::get('oauth/google', [LoginController::class, 'oAuthGoogle'])->name('oauth/google');
Route::get('oauth/redirect', [LoginController::class, 'redirectOauth'])->name('oauth/redirect');

Route::post('/position', [PositionsController::class, 'store'])->name('position');
Route::get('/skeleton', function () {
    return view('skeleton');
});
# Email verification & Notification
Route::get('email/verified', [RegisterController::class, 'verified'])->name('verification.notice');
Route::get('email/message', [RegisterController::class, 'messageEmail']);
Route::get('email/verifying', [RegisterController::class, 'verifying']);
Route::get('email/resending', [RegisterController::class, 'resendingEmail']);
# Auth
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');
Route::post('/login', [LoginController::class, 'authenticate']);
Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::get('/register', [RegisterController::class, 'index']);
Route::post('/register', [RegisterController::class, 'register']);
Route::get('/register/google', [RegisterController::class, 'redirectToGoogle']);
Route::get('/merk/data', [MerkController::class, 'data']);
