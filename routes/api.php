<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Master\UserController;
use App\Http\Controllers\Master\VendorController;
use Illuminate\Http\Request;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', [UserController::class, 'getAll']);
Route::middleware(['auth:api'])->group(function () {
    Route::get('/usernew', [UserController::class, 'getAll_'])->name('usernew');
    # Vendor
    Route::post('/vendor', [VendorController::class, 'store']);
    Route::get('/vendor/{id}', [VendorController::class, 'allData']);

});
Route::post('/login', [LoginController::class, 'authenticate']);
// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
