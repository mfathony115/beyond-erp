<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    {{-- <meta charset="utf-8"> <!-- utf-8 works for most cases --> --}}
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title>Kondisi Usahaku - Verification Notice</title>
	<link rel="shortcut icon" type="png" href="{{ asset('img/logo-beyond-erp.png') }}">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">

    <!-- CSS Reset : BEGIN -->
    <style type="text/css">

	/* What it does: Remove spaces around the email design added by some email clients. */
	/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
	html,
	body {
		margin: 0 auto !important;
		padding: 0 !important;
		height: 100% !important;
		width: 100% !important;
		background: #f1f1f1;
	}

	/* What it does: Stops email clients resizing small text. */
	* {
		-ms-text-size-adjust: 100%;
		-webkit-text-size-adjust: 100%;
	}

	/* What it does: Centers email on Android 4.4 */
	div[style*="margin: 16px 0"] {
		margin: 0 !important;
	}

	/* What it does: Stops Outlook from adding extra spacing to tables. */
	table,
	td {
		mso-table-lspace: 0pt !important;
		mso-table-rspace: 0pt !important;
	}

	/* What it does: Fixes webkit padding issue. */
	table {
		border-spacing: 0 !important;
		border-collapse: collapse !important;
		table-layout: fixed !important;
		margin: 0 auto !important;
	}

	/* What it does: Uses a better rendering method when resizing images in IE. */
	img {
		-ms-interpolation-mode:bicubic;
	}

	/* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
	a {
		text-decoration: none;
	}

	/* What it does: A work-around for email clients meddling in triggered links. */
	*[x-apple-data-detectors],  /* iOS */
	.unstyle-auto-detected-links *,
	.aBn {
		border-bottom: 0 !important;
		cursor: default !important;
		color: inherit !important;
		text-decoration: none !important;
		font-size: inherit !important;
		font-family: inherit !important;
		font-weight: inherit !important;
		line-height: inherit !important;
	}

	/* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
	.a6S {
		display: none !important;
		opacity: 0.01 !important;
	}

	/* What it does: Prevents Gmail from changing the text color in conversation threads. */
	.im {
		color: inherit !important;
	}

	/* If the above doesn't work, add a .g-img class to any image in question. */
	img.g-img + div {
		display: none !important;
	}

	/* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
	/* Create one of these media queries for each additional viewport size you'd like to fix */

	/* iPhone 4, 4S, 5, 5S, 5C, and 5SE */
	@media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
		u ~ div .email-container {
			min-width: 320px !important;
		}
	}
	/* iPhone 6, 6S, 7, 8, and X */
	@media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
		u ~ div .email-container {
			min-width: 375px !important;
		}
	}
	/* iPhone 6+, 7+, and 8+ */
	@media only screen and (min-device-width: 414px) {
		u ~ div .email-container {
			min-width: 414px !important;
		}
	}

    </style>

    <!-- CSS Reset : END -->

    <!-- Progressive Enhancements : BEGIN -->
    <style type="text/css">

	.primary{
		background: #30e3ca;
	}
	.bg_white{
		background: #ffffff;
	}
	.bg_light{
		background: #fafafa;
	}
	.bg_black{
		background: #000000;
	}
	.bg_dark{
		background: rgba(0,0,0,.8);
	}
	.email-section{
		padding:2.5em;
	}

	/*BUTTON*/
	.btn{
		padding: 10px 15px;
		display: inline-block;
	}
	.btn.btn-primary{
		border-radius: 5px;
		background: #30e3ca;
		color: #ffffff;
	}
	.btn.btn-white{
		border-radius: 5px;
		background: #ffffff;
		color: #000000;
	}
	.btn.btn-white-outline{
		border-radius: 5px;
		background: transparent;
		border: 1px solid #fff;
		color: #fff;
	}
	.btn.btn-black-outline{
		border-radius: 0px;
		background: transparent;
		border: 2px solid #000;
		color: #000;
		font-weight: 700;
	}

	h1,h2,h3,h4,h5,h6{
		font-family: 'Lato', sans-serif;
		color: #000000;
		margin-top: 0;
		font-weight: 400;
	}

	body{
		font-family: 'Lato', sans-serif;
		font-weight: 400;
		font-size: 15px;
		line-height: 1.8;
		color: rgba(0,0,0,.4);
	}

	a{
		color: #30e3ca;
	}

	table{
	}
	/*LOGO*/

	.logo h1{
		margin: 0;
	}
	.logo h1 a{
		color: #30e3ca;
		font-size: 24px;
		font-weight: 700;
		font-family: 'Lato', sans-serif;
	}

	/*HERO*/
	.hero{
		position: relative;
		z-index: 0;
	}

	.hero .text{
		color: rgba(0,0,0,.3);
	}
	.hero .text h2{
		color: #000;
		font-size: 40px;
		margin-bottom: 0;
		font-weight: 400;
		line-height: 1.4;
	}
	.hero .text h3{
		font-size: 24px;
		font-weight: 300;
	}
	.hero .text h2 span{
		font-weight: 600;
		color: #30e3ca;
	}


	/*HEADING SECTION*/
	.heading-section{
	}
	.heading-section h2{
		color: #000000;
		font-size: 28px;
		margin-top: 0;
		line-height: 1.4;
		font-weight: 400;
	}
	.heading-section .subheading{
		margin-bottom: 20px !important;
		display: inline-block;
		font-size: 13px;
		text-transform: uppercase;
		letter-spacing: 2px;
		color: rgba(0,0,0,.4);
		position: relative;
	}
	.heading-section .subheading::after{
		position: absolute;
		left: 0;
		right: 0;
		bottom: -10px;
		content: '';
		width: 100%;
		height: 2px;
		background: #30e3ca;
		margin: 0 auto;
	}

	.heading-section-white{
		color: rgba(255,255,255,.8);
	}
	.heading-section-white h2{
		font-family: 
		line-height: 1;
		padding-bottom: 0;
	}
	.heading-section-white h2{
		color: #ffffff;
	}
	.heading-section-white .subheading{
		margin-bottom: 0;
		display: inline-block;
		font-size: 13px;
		text-transform: uppercase;
		letter-spacing: 2px;
		color: rgba(255,255,255,.4);
	}


	ul.social{
		padding: 0;
	}
	ul.social li{
		display: inline-block;
		margin-right: 10px;
	}

	/*FOOTER*/

	.footer{
		border-top: 1px solid rgba(0,0,0,.05);
		color: rgba(0,0,0,.5);
	}
	.footer .heading{
		color: #000;
		font-size: 20px;
	}
	.footer ul{
		margin: 0;
		padding: 0;
	}
	.footer ul li{
		list-style: none;
		margin-bottom: 10px;
	}
	.footer ul li a{
		color: rgba(0,0,0,1);
	}

	@media screen and (max-width: 500px) {


	}
    </style>


</head>

<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #f1f1f1;">
  <center style="width: 100%; background-color: #f1f1f1;">
    <div style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
      &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
    </div>
    <div style="max-width: 600px; margin: 0 auto;" class="email-container">
    	<!-- BEGIN BODY -->
      <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
      	<tr>
          <td valign="top" class="bg_white" style="padding: 1em 2.5em 0 2.5em;">
          	<table role="presentation" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
          		<tr>
          			<td class="logo" style="text-align: center;">
			            <h1><a href="#">kondisi usahaku</a></h1>
			          </td>
          		</tr>
          	</table>
          </td>
	      </tr><!-- end tr -->
	      <tr>
          <td valign="middle" class="hero bg_white" style="padding: 3em 0 2em 0;">
             <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAIACAYAAAD0eNT6AAAgAElEQVR4Xu3dB7RsWVXu8RYEiS05I40iSUEReKigiGIEFKSVLI1Efab3QPCBGBBQdKgD1IcKQoPkB6KICSQINklRBMkgDWJLzjQZ35xwTnv79rn3VO25165atX5rjDn69K01V/ivuWt/tfcKX3KChAACCCCAAALDEfiS4XqswwgggAACCCBwAgEgCBBAAAEEEBiQAAEw4KDrMgIIIIAAAgSAGEAAAQQQQGBAAgTAgIOuywgggAACCBAAYgABBBBAAIEBCRAAAw66LiOAAAIIIEAAiAEEEEAAAQQGJEAADDjouowAAggggAABIAYQQAABBBAYkAABMOCg6zICCCCAAAIEgBhAAAEEEEBgQAIEwICDrssIIIAAAggQAGIAAQQQQACBAQkQAAMOui4jgAACCCBAAIgBBBBAAAEEBiRAAAw46LqMAAIIIIAAASAGEEAAAQQQGJAAATDgoOsyAggggAACBIAYQAABBBBAYEACBMCAg67LCCCAAAIIEABiAAEEEEAAgQEJEAADDrouI4AAAgggQACIAQQQQAABBAYkQAAMOOi6jAACCCCAAAEgBhBAAAEEEBiQAAEw4KDrMgIIIIAAAgSAGEAAAQQQQGBAAgTAgIOuywgggAACCBAAYgABBBBAAIEBCRAAAw66LiOAAAIIIEAAiAEEEEAAAQQGJEAADDjouowAAggggAABIAYQQAABBBAYkAABMOCg6zICCCCAAAIEgBhAAAEEEEBgQAIEwICDrssIIIAAAggQAGIAAQQQQACBAQkQAAMOui4jgAACCCBAAIgBBBBAAAEEBiRAAAw46LqMAAIIIIAAASAGEEAAAQQQGJAAATDgoOsyAggggAACBIAYQAABBBBAYEACBMCAg67LCCCAAAIIEABiAAEEEEAAgQEJEAADDrouI4AAAgggQACIAQQQQAABBAYkQAAMOOi6jAACCCCAAAEgBhBAAAEEEBiQAAEw4KDrMgIIIIAAAgSAGEAAAQQQQGBAAgTAgIOuywgggAACCBAAYgABBBBAAIEBCRAAAw66LiOAAAIIIEAAiAEEEEAAAQQGJEAADDjouowAAggggAABIAYQQAABBBAYkAABMOCg6zICCCCAAAIEgBhAAAEEEEBgQAIEwICDrssIIIAAAggQAGIAAQQQQACBAQkQAAMOui4jgAACCCBAAIgBBBBAAAEEBiRAAAw46LqMAAIIIIAAASAGEEAAAQQQGJAAATDgoOsyAggggAACBIAYQAABBBBAYEACBMCAg67LCCCAAAIIEABiAAEEEEAAgQEJEAADDrouI4AAAgggQACIAQQQQAABBAYkQAAMOOi6jAACCCCAAAEgBhBAAAEEEBiQAAEw4KDrMgIIIIAAAgSAGEAAAQQQQGBAAgTAgIOuywgggAACCBAAYgABBBBAAIEBCRAAAw66LiOAAAIIIEAAiAEEEEAAAQQGJEAADDjouowAAggggAABIAYQQAABBBAYkAABMOCg6zICCCCAAAIEgBhAAAEEEEBgQAIEwICDrssIIIAAAggQAGIAAQQQQACBAQkQAAMOui4jgAACCCBAAIgBBBBAAAEEBiRAAAw46LqMAAIIIIAAASAGEEAAAQQQGJAAATDgoOsyAggggAACBIAYQAABBBBAYEACBMCAg67LCCCAAAIIEABiAAEEEEAAgQEJEAADDrouI4AAAgggQACIAQQQQAABBAYkQAAMOOi6jAACCCCAAAEgBhBAAAEEEBiQAAEw4KDrMgIIIIAAAgSAGEAAAQQQQGBAAgTAgIOuywgggAACCBAAYgABBBBAAIEBCRAAAw66LiOAAAIIIEAAiAEEEEAAAQQGJEAADDjouowAAggggAABIAYQQAABBBAYkAABMOCg6zICCCCAAAIEgBhAAAEEEEBgQAIEwICDrssIIIAAAggQAGIAAQQQQACBAQkQAAMOui4jgAACCCBAAIgBBBBAAAEEBiRAAAw46LqMAAIIIIAAASAGEEAAAQQQGJAAATDgoOsyAggggAACBIAYQAABBBBAYEACBMCAg67LCCCAAAIIEABiAAEEEEAAgQEJEAADDrouI4AAAgggQACIAQQQQAABBAYkQAAMOOi6jAACCCCAAAEgBhBAAAEEEBiQAAEw4KDrMgIIIIAAAgSAGEAAAQQQQGBAAgTAgIOuywgggAACCBAAYgABBBBAAIEBCRAAAw66LiOAAAIIIEAAnHDCfwkDBBBAAIEhCQx9Dxy683vhTgAMed3rNAIIIHDC0PfAoTtPALj8EUAAgaEJDH0PHLrzBMDQF77OI4AAAkPfA4fuPAHg6kcAAQSGJjD0PXDozhMAQ1/4Oo8AAggMfQ8cuvMEgKsfAQQQGJrA0PfAoTtPAAx94es8AgggMPQ9cOjOEwCufgQQQGBoAkPfA4fuPAEw9IWv8wgggMDQ98ChO08AuPoRQACBoQkMfQ8cuvMEwNAXvs4jgAACQ98Dh+48AeDqRwABBIYmMPQ9cOjOEwBDX/g6jwACCAx9Dxy68wSAqx8BBBAYmsDQ98ChO08ADH3h6zwCCCAw9D1w6M5vUAC8OOq+bdgZrj8EEEBgcAKXiv4/MeymG+Aw9D1w6M5vUABk1e8Nu2PYczYQ9KpEAAEEtoHAjaMRTw677IYaM/Q9cOjOb1gAZPX/FfbrYfcP+/yGLgDVIoAAAksTyHvPfcMeEnbupSs/or6h74FDd34LBMB+HD4//rh92Ls3eCGoGgEEEFiCwMWjkj8O+94lKjukjqHvgUN3fosEQDblnWE5L+C0LbgoNAEBBBBoQeB6Uej/CzupReETyhz6Hjh057dMAGRzPhv282H5WiBfD0gIIIDArhC4R3Tkd8LOu0UdGvoeOHTnt1AA7F8Xz4o/Tgn74BZdKJqCAAIITCFwYjg9OuyHpjg39hn6Hjh057dYAGTT3h72w2GvaHwBKB4BBBBoReA6UfDTwq7SqoJiuUPfA4fu/JYLgGzep8LuF/bwYpBzRwABBJYm8CNR4SPDLrB0xWvUN/Q9cOjOdyAA9uM4N8m4Z9jH1whsWRFAAIFNEDh/VJrv+u+6icrXrHPoe+DQnZ9JAOSkvV8Oa72W9XVRx8lhr18zwGVHAAEEliJwjajo6WHXbFzh56L8Xwx7cLGeoe+BQ3d+JgGQDJfazeoTUddPhD2mGPTcEUAAgbkJ3CEK/P2wC81d8FHl5S6qWddzw6qrpYa+Bw7d+RkFQBa15H7WuYlGvhJIQSAhgAACmyTwZVF5Ll3+qQUa8aKo43Zh++eoEAAF6ATAvAoyXwM8cM/OVRiXVVz/OTLlspq3rpJZHgQQQKABga+IMnOW/w0alH1kkXmjz3kF9wn7zBEfEAAF8ATAvAJgfyhuFn88PuxihbFZxfUjkeluYbmzloQAAggsSeAWUdnjwi7auNL3R/m5ouAvD6iHACjAJwDaCIAckiuGPTXsmwrjs4rrvjL+2cj86VUc5EEAAQQKBL40fHPycz7tbP2k8x+jjtwP5W3HaC8BUBhIAqCdAMhhyQslZ6nmqVetWf/D3oVyeiEeuCKAAALHI3D5vR82N1wA0x9GHT95yA8bAqAwEK1vSoWmLea6RADdMnrz2LCLNO7V+6L8O4X9deN6FI8AAuMRuEl0+Ulhl2nc9Y9G+flqM+cWHJaW+P4+rA3dfk4AtH0CcGRgfHX8T76r/7rG0ZIXRM7IfUBYrpWVEEAAgQqBvE/kU8yHhLXe7+RVUUdObn7Lig0mAFYEdVA2AmA5AZD8zxf2sLAllsu8MOrJ5TLvKsQHVwQQGJvAJaL7Twj77gUw5PLme4WduUZdBMAasI7OSgAsKwD2+edj+twj+4KFsVvF9T2R6fZhz1slszwIIIDAEQS+Jf5+StjlGlPJ/UzyR1GeGLhuIgDWJXZEfgJgMwIgh+DqYbll5tcUxm8V189Gpnx096Cwz6/iIA8CCAxNIO8LeUP+jbDzNCbxxig/H/m/ZmI9BMBEcOlGAGxOACT/3DLzUWG3LYzhqq7Pjox3DvvAqg7yIYDAcAROjB7nVuO3XqDnechZPvL/WKEuAqAAjwDYrADYH7p7xB+PCMstNVumd0Thtwl7WctKlI0AAl0S+IZodU5U/srGrZ/zmHMCoDBYBMB2CIAcwh4vvkLocUUAgS0ikDvt5UE+eZRvy/T2vR8hL5+pEgKgAJIA2B4BkMO45OO3Z0Z9dwn7cCF+uCKAQN8E8jVkbriTK4Zap2dFBaeEfXDGigiAAkwCYLsEQA7lkhNw3hT15QScVxdiiCsCCPRJYMmJyLl1cO5PUr1hH026Wt7Q98ChO78XSdsaQEstwflkcMgZvzkZUUIAgTEILLUU+Z2BMyc5n9YI67Z+fzfq7rzFEgB1RdqS4SVjuHNzjG3dhGPeaFQaAgi0JrDkZmTPj87kPiTvbtgpAqAAt+XNq9CsRV23PYD2t+F8aFBpffLW66OOk8Net+gIqAwBBJYgsPR25PePTrXee2Tbv7+XGNfJdRAA2/0E4MiBzYM4nhx26cmjvZpjHsRx97A8ylhCAIHdIPAD0Y1Tw1ofSPbeqOOOYc9ZCBsBUABNAPQjAHKYrxCWW3Nuy1GchdDjigACCxBY8kjyF0V/cjXBGQv0a78KAqAAmwDoSwDkUOcFnTNqfyGs9fi9MurIVQJvK8QYVwQQ2AyBK0a1+STvmxpXnzfh3wm7T9hnGtd1dPEEQAF46xtIoWmLufYaQN8fhE4Nu2hjUu+P8nOTkL9sXI/iEUBgPgLfEUU9KexS8xV5YEm5j8iPhv1J43qOVXyv398bwnX2agmA/p4AHDmCV4r/SYV/g8bRtEmF37hrikdgpwicO3rzwD1rPWl4G54QEgCF8CUA+hYAOfR5fkBusJFr+VunTbzja90n5SOwKwRy2XAesPOdC3Qodw/8ybBPL1DX8aogAAoDQAD0LwD2h/8O8Ufu5Z1be7ZMOcs363puy0qUjQACaxG4ceTOVUKXXctr/czbtkqIAFh/DM/yIAB2RwDkoF4tLE/zulYhJlZx/VxkenDYg8Jar/NdpT3yIDAqgSW3Ds/9QXJS8DbtE0IAFCKfANgtAZChkKd55YzcuxbiYlXX50XGfBrQcqevVdsiHwKjEbh4dPjxYd+3QMdzR9J7hZ25QF3rVEEArEPrqLwEwO4JgP0hzpn7jwy7QCE+VnHNvb5vE/aSVTLLgwACsxC4XpTytLArz1LasQvJs0LyXf+jG9cztXgCYCq58CMAdlcAZFhcZ+9L4iqFGFnF9bORqdVpX6vULw8CIxG4R3Q2n/Kdt3Gn3xjl5yP/1zSup1I8AVCgRwDstgDI0DhxT73nhdw6/VlUcErYh1pXpHwEBiRw4ehzntqZT9xap1zXn+v7c53/NicCoDA6BMDuC4D98FjqV8Obo8IfDntVIS65IoDA2QlcM/43J/jmf1umT0Xh9wt7eMtKZiybACjAJADGEQAZJtcPy/eGJxViZhXXfG/4cx19iazSJ3kQ2BSBpebzvGNPvL98Ux2dUC8BMAHavgsBMJYAyHG/RFjOHP7eQtys6vqEyJgzhz++qoN8CCBwFoHzxV/5rv9uCzD586jjzmEfXKCuOasgAAo0CYDxBECGS477fcMeEpZbh7ZMb4jCc/7Bv7asRNkI7BiBq0Z/nh7Wek+PnMCb3wO97ulBABQCnwAYUwDsh8y3xR95YEjr3cM+FnXkHITcqUxCAIHjE7hVfPzYsC9vDOo/ovycUHha43paFk8AFOgSAGMLgAydPC0s9w+/aSGOVnXNzUTuGfaJVR3kQ2AgAkue6/GC4Hr7sHd1zpcAKAwgAUAAZPgseYLYP0V9uUrgrYW45YrArhH4iuhQnuz5jY07ljfMPDzs/mG7sI03AVAIGAKAADgyfG4e//O4sIsVYmoV149EptyqON9xSgiMTuAWAeDUBa67PMjrjmHP2SHgBEBhMAkAAuDo8Lli/EMuFVzil0jOcL5P2GcKMcwVgV4JfGk0PHfQfGDYuRp34sVR/m3Dzmhcz9LFEwAF4gQAAXBQ+OQXU572lxuCtE6viArylcDbW1ekfAS2iMDloy1PCbtR4zblDXKXhTYBUAggAoAAOF745Gzkx4RdpBBjq7i+LzLlo8m/WSWzPAh0TuAm0f5cfXOZxv3IbXzzVdszGtezyeIJgAJ9AoAAOCx8cj1ybkF67cMyFj/fn5z0gCjnc8WyuCOwjQSW3H/jlQEgn6z92zaCmLFNBEABJgFAAKwSPrkj2SPC7r5K5mKeXVmeVMTAfccI5A6cuQz2exbo1x9GHXmE76cXqGvTVRAAhREgAAiAdcJnqT3Jc4OSnLD09+s0Tl4EtpTAUmdwfDT6nxtu5dyCURIBUBhpAoAAWDd8rhEOuXyv9alkvW9Rui5X+XePQH6//lTYb4Sdp3H3Xh/lnxz2usb1bFvxBEBhRAgAAmBK+OS55PmYMX+lt069HlLSmovyt5vAidG8P9q7Kbduab5ayEO3zmxd0RaWTwAUBoUAIAAK4fOFx425xOi8lUJW8O3xmNIVuiXLjhK4TvQrJ85+VeP+5bHb+YThUY3r2ebiCYDC6BAABEAhfL7get29L7srVws6xP9T8XnuS/DwxvUoHoEKgZwn8/th568UsoLvmyJPnrL56hXy7nIWAqAwugQAAVAIn7Nc89Sy3C/gB+co7JAy/iQ+/9GwXOMsIbAtBC4UDfmDsDxgp3V6ZlRwF9fAFzATAIVoIwAEUCF8zua65ISnN0bN+evnNXM1XjkIFAhcPXzzkf/XFspYxdVTsHNSIgBWiZxj5CEACIBC+Bzo+q3xr08Ou9zcBR9VXh4pnO8/H924HsUjcDwCuYNlPvK/YGNMOQ/mNmEva1xPb8UTAIURIwAIgEL4HNP1kvHJE8K+q0XhR5U58gzoBfCq4hgEcnOsh+2J0NaQciXMKWEfaF1Rh+UTAIVBIwAIgEL4HNf13PFpnnK2xElnr4p68pXAW1p1RrkIHEHgSvF3npj5PxpTsRfG4YAJgMMZHTMHAUAAFMJnJddvj1x58MmlV8o9PVPugna3vS/m6aXwROD4BH4gPn5s2EUbg7Ib5mqACYDVOB2YiwAgAArhs7LrFSLnU8O+eWWP6RlH2gd9OiWe6xLYPyL7vuHY+nvTeRirjw4BsDqrc+RsHciFpi3mKoCWQb3kF+g/RpfyJLS3LdM1tew4gaUErBMx1w8k39/rMzvLgwDwBKAQPpNcl3qE+v5o3Z3C/mpSKzkh8EUCS73Cel/UlSsK/gb4tQgQAGvhOntmAoAAKITPZNelJlHll0NuVXzvsJxQJSGwKoElJ7G+OBqV52qcsWrj5DuLAAFQCAYCgAAohE/JdcllVH8XLb1d2H+WWsx5FAJLLWPdF6j3CbCfGQXuzP0kAApACQACoBA+s7gutZHKe6K1dwj721larZBdJbDURlYfCYC5pfUzdhXkQv0iAAqgCQACoBA+s7kutZXq56LFDw57UNjnZ2u9gnaBwJJbWf9TAMt9K/5tF8BtuA8EQGEACAACoBA+s7oueZjKX0TL89Q2O6vNOoTdFrbkYVa5TDW3sM59/aU6AQKgwJAAIAAK4dPEdanjVP89Wp97q7+0SS8U2guBpY6zzo2q7hH2lF7AdNJOAqAwUAQAAVAIn2au14mS83S1r2pWwxcLzpUBPx+We7pL4xHIG3KuEjlv466/PsrPR/6vbVzPiMUTAIVRJwAIgEL4NHU9MUr/o7CTm9byxcL/NCzPV//QAnWpYvMELhxNyEfxufSudcrDqn4s7OOtKxq0fAKgMPAEAAFQCJ/mrktOzHrz3q+0f2neKxVsksA1ovKnh12zcSM+GeX/9J7QaFzV0MUTAIXhJwAIgEL4LOZ6/agpXwnkBkItU35p/1zYw1tWouyNEcj5JY8Mu0DjFrxpT0y+unE9ivf9XYoBAkAAlQJoQedLRF35OPV7FqjTY9sFIC9YRW469Yiwuy9Q5zOjjlzf73XSArCjCk8ACpwJAAFUCJ/FXTNe8zS2h4TlVq0tk4lbLekuV/ZVo6p8enTtxlXmsr77hXl61Bj0UcUTAAXeBAABUAifjbneJGp+UthlGrfA0q3GgBsXf6so/zFhF2lczzui/FxS+rLG9Sj+nAQIgEJUEAAEQCF8Nup6+ag911TfaIFW2LxlAcgzVrF/9HT+Im+dnh0V3DnMplKtSR9cPgFQ4E4AEACF8Nm4a37R5zr+B4adq3FrbN/aGPBMxV8xynla2DfOVN6xisk9JPJVlG2lG4M+pHgCoMCfACAACuGzNa63iJacGnaxxi36cJR/1zAHuDQGPbH4m4ff4xaIgzxY6vZhz5vYTm7zESAACiwJAAKgED5b5foV0ZqnLvDLzxGuWzXsX2hMTgjNp0BLPAl6wd7N/13bh2HIFhEAhWEnAAiAQvhsneuXRYt+PSwPW2mdXhwV5E5yZ7SuSPnHJXCp+PSJYTdtzClvNBlbDwjLUyWl7SBAABTGgQAgAArhs7WuOfv7sWF5ylvL9N4o/I5hz2lZibKPSeDb4pNcDXLZxozeF+XfKeyvG9ej+PUJEADrMzvLgwAgAArhs9Wuuf47t3y9VuNW7v8yvH/U8/nGdSn+iwSW3A/iFVHfD4e9HfytJEAAFIaFACAACuGz9a65A1ye9na3BVr6/KgjJ4a9e4G6Rq4id4R8fNj3NoZgrkdjwDMVTwAUQBIABEAhfLpxXWoP+HcGkZwXcFo3ZPpqaJ4JkUv8Tmrc7I9E+bnaI58gSdtNgAAojA8BQAAUwqcr16+P1uaWsFdp3OpcH557E+SEseqXU+OmdlX8PaK1+TTnvI1bnfs95CP/tzauR/HzEKheY0PfA4fu/F78CaB5LsQeSslz4B8Vltu2tk7PigpOCftg64p2vPwTo3+PDvuhBfqZh0DdM+wTC9SlinkI+P4ucCQA6r/SMCwE4IZcl/o1mRPH8tdkTiST1idwnXDJR/6tn9p8LOrImHjy+k3ksWECBEBhANy8CIBC+HTter29m8uVG/fCKXHTAC81b+MN0byTw147rZm8NkyAACgMAAFAABTCp3vXi0cPckb59y3Qk9ys5l5h+WtTOjaB88dH+a4/J+G1TvnI/8fCPt66IuU3I0AAFNASAARAIXx2wjWvgdw58DfCztO4R2+M8vPX5r82rqfX4q8WDc+Jmq33bvhk1PFzYQ/vFZR2n0WAACgEAwFAABTCZ6dcvzN681dhua98y5S/NnOiWT4RkP6bwB3izz8Iu2BjKLlZ0zeHvbxxPYpfhgABUOBMABAAhfDZKdffi978+II9ysfP+UrgzAXr3Maqljy/Yb//z44/bhlmT/9tjIj12kQArMfrbLkJAAKgED4745qnyOW57kunf44Kc5XAW5aueEvqu1K0I09wvMEG2vP7UWe+/5f6JkAAFMaPACAACuGzE663i17k4/hNXQu561xuVZzvvkdK3x+dPTXsohvs9L2j7t/aYP2qrhMgAAoMN/WlV2jy7K4CaHak3RR4k2hpvvfPx9CbTPv7zv9sNOLTm2zIAnV/adSROyX+wgZF1343k3suN3zCAv1WRRsCvr8LXAkATwAK4dO169dG618cdpEt6sU/RFvylcDpW9SmOZtyhSjsKWE3nLPQYlkpuPJgoTzMSeqPAAFQGDMCgAAohE+3rpePlr807Ipb2INdPXs+n7bkTnuX3kLmH442fUvYa7awbZp0fAIEQCFCCAACoBA+Xbrm3vL5y//aW9z6/FLLw4QeENb7TPX8jrlv2EPDzrXFzE+Ptn1T2Lu2uI2adk4CBEAhKggAAqAQPt255kY/fxl2005a/sJo5+3D/rOT9h7dzEvGP+Ryx+/upP15EuCNw+zW2MmARTMJgMJYEQACqBA+XblmrD8u7E5dtfqEE96zJwKe11m785F6vu+/XGftzkmhuUIhj3WWtp8AAVAYIwKAACiET1euvxatvd/MLc6bcv7Kbf06IW9G+TogtyuufuHNjOAcxeV3Sq5meEhYzvhvmV4dhb837DtmriSPH777zGUqrg2B6vUw9D1w6M7vxaMAanNhblOpedRrbjM7Z8pDhHL9fm4d/LCwPE+gdcod7O4c9oHWFU0sP+dXPCbs1hP913Hb30kxZ/H/3wY37Nwc6sHrNEjejRDw/V3ATgDUf1FhWAjABVxvFnX8adicv0YfEeX9TNiRXz65njxvRK33sv/3qCOXCr5sAXbrVPENkTk3M/rKdZwm5M2DfH467A+P8M1r8Bf3bEKRB7rk2P5o2KlzFaicJgQIgAJWNy8CoBA+W+96/WjhC2a8KeeM/J8Iy21kD0rX2LsJfk1jMp+J8vMXaj552IaUT1hSFLXeUOlNUccPheWj/4PSXeIf80nPXKc6Juebhz1nGyBrw4EECIBCYBAABEAhfLbaNX+JviRsrnXneYrfbcPyMfzx0oXjw/x1mnlbp2dGBXnTy3Xsm0gXikoftWBf8xf5hw7paJ7q+IywHIc50kejkG8Ne9UchSljdgIEQAEpAUAAFMJna10vES07LeyqM7Xw/VFOzgxPQbFq2pZfxau2d918Vw+Hp4e1ftqREyBz6+B1nnZcL/KnUJtL/J0RZeUeAe9YF5L8zQkQAAXEBAABUAifrXQ9f7Tqb8PyzPc50lujkNwq9s0TClvyvXhOQsxf40ukXEr5yLBtnu9w5Whf7vmQQmWO9Loo5EZhH5yjMGXMRoAAKKAkAAiAQvhsnWvuNJcT0X5wppa9PMq5RVguNZuavjwc/yhsyZnxZ05t7CF+54vPe1rxcLFob04AzT0J5kh/F4XkpkafmqMwZcxCgAAoYCQACIBC+Gyda05E+8mZWpU3jtyF7xMzlJfXWf5Cz3X8c01QO1azXh8fnByWv1jnTF8dhaW4+ro5Cz2grJxomcvvHhT2+RnqyomJuQHUbWYoK4vIzY0yLqo3ngsiSVAAABoNSURBVJmaM3wx1XEY+h44dOf3Lh0BtBvfIbnJT272M0f6nSgkl/nNcQM6sj1L7Y6XE9dyI5unzgEjyrhl2GPDWp+cmLse3iEsX+HMmfJ7Lp9c5AZFc6RfjULuP0dByigT8P1dQEgA1JU8hoUAnMk1f909Kax62Ex+meQvz1+aqV0HFZM7B+b589/VsI79onM1Qj4Ryc1ypqTcOyF/jedhPq3j/IVRR+tzD3L/gN+aIU6SZXL93SlQ+cxKgAAo4Gx9UReatpirAFoMdZOKcolWrtOurkHP97q5pC6PrG2dljwh75XRmVw7/7Y1O5VHJecThJz93jItffJhzg9JAZaTRSspX1Xkq5Z8VSRtjoDv7wJ7AsATgEL4bNz1mtGCvw+7aLElObP7VmE5yWvJdJOoLAXHXMvVjtX23Ccg19D/yYqdy90Tc6vjnETXMr0vCs8VBX/dspIDyk5R86ywXC5aSTk/JM8heGmlEL4lAgRAAR8BQAAUwmejrnnKXK7Lv1KxFf8R/nnD+5diOVPdrxCOObHshlMLWNEvvyhzbsN9wnKHu4NSnmuQOwymVV+nHNasf4gMuaXx6YdlbPR5isdcJliNnxQxueR0yjLRRl0bqlgCoDDcBAABUAifjbnmLm8vCvv6YgteE/7fF/bOYjlV93zXnpvd/EJY62syud0uLDe3OTJdKv7niWE3rXbmEP99IZIT8qbOTZiriZeNgnLDoNyvoZJyr4gUATmJUVqWAAFQ4N36y6bQtMVcBdBiqGepKJfR5Zd2dRJdHuWba/M3tY3uQTByt8FTw6qvNA4Dnfsa5Gz75+5lvHH8N19F5A2xZfpIFJ4nKOZywm1JuZ3x08Jys6dKekU45yudVnswVNq2y76+vwujSwB4AlAIn8VdM15zOdqdizXnuvBcJnesR+HF4kvuX7V3Q6r+Kj2sETmJLU/Qy/TLYfn4v2X6pyg8H/nnr+VtSykqcxfFalzlHIuccDn38tFt47VN7SEACqNBABAAhfBZ3PVXosZ8VF5JBx3lWymvhW+uaPj1sNw8aBfSH0cn7hW2zb+O87swBdG+KJrKPU+K/LGpzvzWJkAArI3svx0IAAKgED6Lut41ant0ocbDjvItFN3MNR/T5w0lH1P3mHKWfK6Xz62Qe0mnRENz/4TKjo33Dv/cb0BqT4AAKDAmAAiAQvgs5prvZ3PZVk6Wm5JWPcp3Stmtfa4WFeQ782u1rmjm8t8Y5eU6+X+dudwlissjhfOkwxMnVpY3pVzemJMqpbYECIACXwKAACiEzyKu141aXhg29Vfwu8P35mH/uEhr21SSm9bkEr58CtJDyhtfPvL/WA+NPUYbrx3/nssELz+xD7nCIYXr8yf6c1uNAAGwGqcDcxEABEAhfJq7nhQ15CYrl5lYU+Uo34lVNnX7kSg9j+G9QNNapheeuynmmQwPn17EVnleeU8ETD1S+APhn/s7vGGrerVbjSEACuNJABAAhfBp6pq70J0WNvXL92Xhm8vqKkf5Nu3gxMKvE365bO0qE/1bub09Cs5Z/rkcbpdSLsn8s7CpRwqfHr658+C7dgnKFvWFACgMBgFAABTCp5lrnjufa9RvNLGGOY/yndiEpm75bjonROaSs21IOT/jlLDcUnkXU/VI4VwCmXst9PxKZFvHlQAojAwBQAAUwqeJa25Bm4fQ5ASyKSmX+f2vsBHWYt8j+plzA847BdQMPp+NMnJZZi5ZrH4Rz9CcpkXkd2UeN50nI05JfxVO+UQqmUnzEajG3dD3wKE7vxeDAmi+i3GOkn47CvmZCQXlOLY+yndCs5q7XD9qyFcCJzWv6ewV5PbJtw3L1zQjpcqRwrkcMndClOYj4Pu7wJIAqP9ywbAQgEe55i/3KeunlzzKd77ezlfSxaOo3Gynup3tqi3Kme23D8sVFiOmypHCedDSg0eE1qjPBEABrJsXAVAIn1ld8/FobqW67pa0mzrKd9bOz1BYXsv5ePohExiuWn1+2ebj/vuHjfCK5Xhcph4pnAzzaOZTV4Uu33EJEACFACEACIBC+MzmeoMoKX9Vrru8LY/yzdP8Xj1bS/ovqNXBPrma4o5hz+kf0Ww9uEaUlO/2r7RmiXkGRe5NgeWa4A7ITgAUGBIABEAhfGZxzeVsLwm75JqlbctRvms2e5Hscx/t++Jodb7vP/oI4UU6s+WV5B4VfxG27uFNHw2fbw171Zb3b9ubRwAURogAIAAK4VN2zZt+3vzXXdOeR/nme9g8XlY6mEC+Ssn3zWm5smJKyi/XXGVwn7BtPDlxSp9a+OQulblyJZ9GrZNSUOWrhHes4yTv2QgQAIWAIAAIgEL4lFzzcX/eyL9xzVK2+SjfNbuySPabRS2PD8uNldZJH47MufXwM9ZxGjhvnlPxe2G5NHOd9LrInPtd7OoeCuuwmJKXAJhCbc+HACAACuEz2TV/neaN5QfWLOFhkf//hFUv+jWr7T77FaMH+Qs1f22ukl4ZmXJXv39bJbM8ZyOQWyHnfgHrpL+LzN8dlqtZpPUIVL8Lhr4HDt35vTgTQOtdcHPkzl9KP75GQT0e5btG9xbJmr9Qc/lZ3qCOl/Io3DzCNw+zkaYROCXc1j1S+Cnhk0srq99H01rcr1eV19D3wKE7TwBs5KrPd9K5Yc+qKY/yvU1YTrSS6gRuGUU8NuwiRxWVk9Ly8XXeiKQ6gSlHCj80qn1AveqhSiAACsNNANQVN4arB2D+wnlC2KrM8gCVXC6Vj6Sl+Qh8dRSV593nkbeZXh+W5wq8dr4qlLTHd90jhfPpy++itzIBAmBlVOfMuOoXcaGKrXcVQMsMUf4iyl/x51mxujdGvtzZ7m0r5pdtPQLnj+z5KibT/wz7xHrucq9III8Uzr0CrrZi/nzdlU9pnr1i/tGz+f4uRAAB4AlAIXxWdv3ayJlryY9+7HysAnb1KN+Vgcm4UwTySOE8oTLX/a+SUox9e1heB9LxCRAAhQghAAiAQvis5Hr5yPXSsJyJvkp6ZmS6Q5hfpKvQkqcXAnmk8KlhuaHSKul9kembw968SuaB8xAAhcEnAAiAQvgc6prn1ucv//13zYc5jHSU72EsfL57BNY9UviteyLgPbuHYrYeEQAFlAQAAVAIn+O65hn1+c7/pitUMOpRviugkWUHCaxzpPArov83CTtzBznM0SUCoECRACAACuFzTNeMq9yx704rFD76Ub4rIJJlBwmsc6Twn0f/bxWWEwSlsxMgAAoRQQAQAIXwOaZr7tiXR9MelnL705zx/KLDMvocgR0kkNtg5839Eiv07ZGRZ53Ns1YocieyEACFYSQACIBC+BzompvJ/MEKhZ4eefLwlFyDLiEwKoE8CCuXCa5yINb/jny/PSqoY/SbACgEBAFAABTC5xyuuWlPzuLPbWePlxzlOyd1ZfVOII8UznX/1z2kI3mzy9dqT+y9wzO2nwAowCQACIBC+JzN9frxfy8Iu+AhBf5tfH7rMEf5zkVeObtAYNUjhT8Znc1Ntf5+Fzo9Qx8IgAJEAoAAKITPWa5fGX+9JOzShxTmKN85aCtjVwnkk7PcBvieh3TwA/H5DcPesKsg1ugXAbAGrKOzEgAEQCF8vuCaE5hOC7vqIQU5yrdKmv8oBPLExl8NO9738+nxeR7vnOdljJwIgMLoEwAEQCF8Tsj95PORfu5YdqyUS5dyr/lVJgZW2sIXgV0icEp05rAjhfOQrG8L+9gudXzNvhAAawI7MjsBQABMDZ9zhWOeKJfrk4+VHOU7lS4/BL64idYzwnJHzWOlXEHw/WGfHRQYAVAYeAKAAJgaPrltbx5deqzkKN+pZPkh8N8Echvtw44UfnTkufug0AiAwsATAATAlPDJd5S/dhzHt8RneZRv/ldCAIEagZPCPX/pX/04xfx8fPaQWjVdehMAhWEjAAiAdcPnNuHwpLB8BXBQcpTvukTlR+BwAocdKZw3wruE5UqbkRIBUBhtAoAAWCd8bhyZ/yYsjzY9KDnKdx2a8iKwHoHDjhT+TBR3s7Dnrlds17kJgMLwEQAEwKrhc83ImJuP5C+Rg5KjfFclKR8C0wnkd3YuEczXcAel3GArhfqrplfRlScBUBguAoAAWCV8LheZcqOfKx2Q2VG+qxCUB4F5CRzvSOEzoqrcI+Ad81a5laURAIVhIQAIgMPC58KRIU/r+/oDMuZRvqeEPeWwQnyOAAKzE8gluHkuQO7HcXR6bfzDjcI+NHut21UgAVAYDwKAADhe+JwnPsxDSr7rgEyO8i1ceFwRmInA8Y4UfmHU8T1hKdR3NREAhZElAAiAY4VPxsZjwk45IMPp8W+O8i1ceFwRmJHA8Y4Uzqdztw+r3ihnbO6sRVX7NfQ9cOjO74WhADr4evyV+OdcW3x0cpTvrN9fCkNgFgLHO1L4oVHDA2apZfsK8f1dGBMCoK6Md5HhXSOmcnexo5OjfAsXG1cEGhPIo7ifGpZLAY9OPxH/8HuN699E8QRAgfou3rzWxSGAzk4sd/B7VlgeTXpkcpTvupElPwLLEzj33o3+6COF81CuW4f92fJNalqj7+8CXgLAE4Ajw+e68T8vDLvQUTHlKN/CRcYVgQ0QOOhI4TOjHd8Rlrt17koiAAojSQAQAPvhc+X4I9f657vE/eQo38LFxRWBDRO4c9T/qLBczbOf3hd/5PHdb95w2+aqngAokCQACIAMn4uFnRZ25GEjjvItXFhcEdgSAgcdKfzWaFtuFPTeLWljpRkEQIEeAUAAnC/iJ/cOz01D9pOjfAsXFVcEtozAtaI9eaTwFY5o1yvi75uE5WuBnhMBUBg9AmBsAZAn+uWs4ZOPiCFH+RYuKK4IbCmBk/ZEwDWOaF9O9v3BsHzV12siAAojRwCMLQB+O2LnZ46In5wcdIuwfE8oIYDAbhE46EjhR0YXf7zjbhIAhcEjAMYWADlT+Nf24sdRvoULiSsCnRA4+kjhX4p2/3InbT+omQRAYfAIgLEFQIZOfgF8edi9wz5fiCWuCCDQB4F89febYR/eu/77aPXBrSQACqNHABAAhfDhigACCGyUAAFQwE8AEACF8OGKAAIIbJQAAVDATwAQAIXw4YoAAghslAABUMBPABAAhfDhigACCGyUAAFQwE8AEACF8OGKAAIIbJQAAVDATwAQAIXw4YoAAghslAABUMBPABAAhfDhigACCGyUAAFQwE8AEACF8OGKAAIIbJQAAVDATwAQAIXw4YoAAghslAABUMBPABAAhfDhigACCGyUAAFQwE8AEACF8OGKAAIIbJQAAVDATwAQAIXw4YoAAghslAABUMBPABAAhfDhigACCGyUAAFQwE8AEACF8OGKAAIIbJQAAVDATwAQAIXw4YoAAghslAABUMBPABAAhfDhigACCGyUAAFQwE8AEACF8OGKAAIIbJQAAVDATwAQAIXw4YoAAghslAABUMBPABAAhfDhigACCGyUAAFQwE8AEACF8OGKAAIIbJQAAVDATwAQAIXw4YoAAghslAABUMBPABAAhfDhigACCGyUAAFQwE8AEACF8OGKAAIIbJQAAVDATwAQAIXw4YoAAghslAABUMBPABAAhfDhigACCGyUAAFQwE8AEACF8OGKAAIIbJQAAVDATwAQAIXw4YoAAghslAABUMBPABAAhfDhigACCGyUAAFQwE8AEACF8OGKAAIIbJQAAVDATwAQAIXw4YoAAghslAABUMBPABAAhfDhigACCGyUAAFQwE8AEACF8OGKAAIIbJQAAVDATwAQAIXw4YoAAghslAABUMBPABAAhfDhigACCGyUAAFQwE8AEACF8OGKAAIIbJQAAVDATwAQAIXw4YoAAghslAABUMBPABAAhfDhigACCGyUAAFQwE8AEACF8OGKAAIIbJQAAVDATwAQAIXw4YoAAghslAABUMBPABAAhfDhigACCGyUAAFQwE8AEACF8OGKAAIIbJQAAVDATwAQAIXw4YoAAghslAABUMBPABAAhfDhigACCGyUAAFQwE8AEACF8OGKAAIIbJQAAVDATwAQAIXw4YoAAghslAABUMBPABAAhfDhigACCGyUAAFQwE8AEACF8OGKAAIIbJQAAVDATwDUBUABP1cEEEAAgQ0SGPoeOHTn94KuqiA3GLuqRgABBBAoEBj6Hjh05wmAwmXDFQEEEOifwND3wKE7TwD0f/XqAQIIIFAgMPQ9cOjOEwCFy4YrAggg0D+Boe+BQ3eeAOj/6tUDBBBAoEBg6Hvg0J0nAAqXDVcEEECgfwJD3wOH7jwB0P/VqwcIIIBAgcDQ98ChO08AFC4brggggED/BIa+Bw7deQKg/6tXDxBAAIECgaHvgUN3ngAoXDZcEUAAgf4JDH0PHLrzBED/V68eIIAAAgUCQ98Dh+48AVC4bLgigAAC/RMY+h44dOcJgP6vXj1AAAEECgSGvgcO3XkCoHDZcEUAAQT6JzD0PXDozhMA/V+9eoAAAggUCAx9Dxy684Wg4YoAAggggEDXBAiArodP4xFAAAEEEJhGgACYxo0XAggggAACXRMgALoePo1HAAEEEEBgGgECYBo3XggggAACCHRNgADoevg0HgEEEEAAgWkECIBp3HghgAACCCDQNQECoOvh03gEEEAAAQSmESAApnHjhQACCCCAQNcECICuh0/jEUAAAQQQmEaAAJjGjRcCCCCAAAJdEyAAuh4+jUcAAQQQQGAaAQJgGjdeCCCAAAIIdE2AAOh6+DQeAQQQQACBaQQIgGnceCGAAAIIINA1AQKg6+HTeAQQQAABBKYRIACmceOFAAIIIIBA1wQIgK6HT+MRQAABBBCYRoAAmMaNFwIIIIAAAl0TIAC6Hj6NRwABBBBAYBoBAmAaN14IIIAAAgh0TYAA6Hr4NB4BBBBAAIFpBAiAadx4IYAAAggg0DUBAqDr4dN4BBBAAAEEphEgAKZx44UAAggggEDXBAiArodP4xFAAAEEEJhGgACYxo0XAggggAACXRMgALoePo1HAAEEEEBgGgECYBo3XggggAACCHRNgADoevg0HgEEEEAAgWkECIBp3HghgAACCCDQNQECoOvh03gEEEAAAQSmESAApnHjhQACCCCAQNcECICuh0/jEUAAAQQQmEaAAJjGjRcCCCCAAAJdEyAAuh4+jUcAAQQQQGAaAQJgGjdeCCCAAAIIdE2AAOh6+DQeAQQQQACBaQQIgGnceCGAAAIIINA1AQKg6+HTeAQQQAABBKYRIACmceOFAAIIIIBA1wQIgK6HT+MRQAABBBCYRoAAmMaNFwIIIIAAAl0TIAC6Hj6NRwABBBBAYBoBAmAaN14IIIAAAgh0TYAA6Hr4NB4BBBBAAIFpBAiAadx4IYAAAggg0DUBAqDr4dN4BBBAAAEEphEgAKZx44UAAggggEDXBAiArodP4xFAAAEEEJhGgACYxo0XAggggAACXRMgALoePo1HAAEEEEBgGgECYBo3XggggAACCHRNgADoevg0HgEEEEAAgWkECIBp3HghgAACCCDQNQECoOvh03gEEEAAAQSmESAApnHjhQACCCCAQNcECICuh0/jEUAAAQQQmEaAAJjGjRcCCCCAAAJdEyAAuh4+jUcAAQQQQGAaAQJgGjdeCCCAAAIIdE2AAOh6+DQeAQQQQACBaQQIgGnceCGAAAIIINA1AQKg6+HTeAQQQAABBKYRIACmceOFAAIIIIBA1wQIgK6HT+MRQAABBBCYRoAAmMaNFwIIIIAAAl0TIAC6Hj6NRwABBBBAYBoBAmAaN14IIIAAAgh0TYAA6Hr4NB4BBBBAAIFpBAiAadx4IYAAAggg0DUBAqDr4dN4BBBAAAEEphEgAKZx44UAAggggEDXBAiArodP4xFAAAEEEJhGgACYxo0XAggggAACXRMgALoePo1HAAEEEEBgGgECYBo3XggggAACCHRNgADoevg0HgEEEEAAgWkECIBp3HghgAACCCDQNQECoOvh03gEEEAAAQSmESAApnHjhQACCCCAQNcECICuh0/jEUAAAQQQmEaAAJjGjRcCCCCAAAJdEyAAuh4+jUcAAQQQQGAaAQJgGjdeCCCAAAIIdE2AAOh6+DQeAQQQQACBaQQIgGnceCGAAAIIINA1AQKg6+HTeAQQQAABBKYRIACmceOFAAIIIIBA1wQIgK6HT+MRQAABBBCYRoAAmMaNFwIIIIAAAl0TIAC6Hj6NRwABBBBAYBoBAmAaN14IIIAAAgh0TYAA6Hr4NB4BBBBAAIFpBAiAadx4IYAAAggg0DUBAqDr4dN4BBBAAAEEphEgAKZx44UAAggggEDXBAiArodP4xFAAAEEEJhGgACYxo0XAggggAACXRMgALoePo1HAAEEEEBgGgECYBo3XggggAACCHRNgADoevg0HgEEEEAAgWkECIBp3HghgAACCCDQNQECoOvh03gEEEAAAQSmESAApnHjhQACCCCAQNcECICuh0/jEUAAAQQQmEaAAJjGjRcCCCCAAAJdEyAAuh4+jUcAAQQQQGAaAQJgGjdeCCCAAAIIdE2AAOh6+DQeAQQQQACBaQQIgGnceCGAAAIIINA1AQKg6+HTeAQQQAABBKYRIACmceOFAAIIIIBA1wQIgK6HT+MRQAABBBCYRoAAmMaNFwIIIIAAAl0TIAC6Hj6NRwABBBBAYBoBAmAaN14IIIAAAgh0TYAA6Hr4NB4BBBBAAIFpBAiAadx4IYAAAggg0DUBAqDr4dN4BBBAAAEEphEgAKZx44UAAggggEDXBAiArodP4xFAAAEEEJhGgACYxo0XAggggAACXRMgALoePo1HAAEEEEBgGgECYBo3XggggAACCHRNgADoevg0HgEEEEAAgWkECIBp3HghgAACCCDQNQECoOvh03gEEEAAAQSmEfj/EauMW6pinqsAAAAASUVORK5CYII="
			 alt="email.png" style="width: 300px; max-width: 600px; height: auto; margin: auto; display: block;">
          </td>
	      </tr><!-- end tr -->
				<tr>
          <td valign="middle" class="hero bg_white" style="padding: 2em 0 4em 0;">
            <table>
            	<tr>
            		<td>
            			<div class="text" style="padding: 0 2.5em; text-align: center;">
            				<h2>Please verify your email</h2>
            				<h3>Get more information, after you verify your account</h3>
							<p>
								<a href="{{ url('email/resending')}}" class="btn btn-primary">Resend email verification</a>
							</p>
            			</div>
            		</td>
            	</tr>
            </table>
          </td>
	      </tr><!-- end tr -->
      <!-- 1 Column Text + Button : END -->
      </table>
    </div>
  </center>
</body>
</html>