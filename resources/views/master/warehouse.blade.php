@extends('layouts.layout')
@section('content')
<div class="container-fluid">
    {{-- modal warehouse --}}
    <div class="modal fade" id="modal-warehouse" tabindex="-1" aria-labelledby="modal-warehouse-label" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-warehouse-label">New Warehouse</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" id="alert">
                </div>
                <div class="col-md-12">
                    <form id="form-menu">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <input type="hidden" class="reset" name="warehouse_id" id="warehouse_id">
                                <label for="warehouse" class="col-form-label">Warehouse :</label>
                                <input type="text" class="form-control reset" name="warehouse" required id="warehouse">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="Address" class="col-form-label">Address :</label>
                                <textarea name="address" id="address" class="form-control reset" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="btn-save" class="btn btn-primary">Save</button>
            </div>
            </div>
        </div>
    </div>
    {{-- end modal warehouse --}}
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">
                Table Warehouse 
                <button data-toggle="modal" id="btn-tambah" data-target="#modal-warehouse" class="btn btn-circle btn-primary btn-sm ml-2">
                    <i class="fas fa-plus"></i>
                </button>
            </h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="data-warehouse" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Warehouse</th>
                            <th>Address</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Warehouse</th>
                            <th>Address</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection
<script src="{{ asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('js/sweetalert.min.js')}}"></script>

<script>
    $(function () {
        const status = `{{ Auth::user()->store_id === 1 ? 'all': 'byStore' }}`;
        const store_id = `{{Auth::user()->store_id}}`;
        let warehouse_id = $('#warehouse_id').val();
        let warehouse = $('#warehouse').val();
        let address = $('#address').val();

        $('#data-warehouse').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
               url: `{{ url('warehouse/data') }}/${status}`,
               type: "POST",
               headers: {
                "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
               },
               credentials: "same-origin"
            },
            columns: [
                { data: 'DT_RowIndex'},
                { data: "warehouse" },
                { data: "address"},
                {
                    data: 'action', 
                    name: 'action', 
                    orderable: true, 
                    searchable: true
                }
            ]
        });

        $('#btn-tambah').on('click', function(){
            $('#modal-warehouse-label').html(`New Warehouse`);
            $('.reset').val('');
        });

        $('#data-warehouse').on('click', '#btn-edit', function(){
            const id = $(this).data('id');
            const warehouse = $(this).data('warehouse');
            const address = $(this).data('address');
        
            $('#warehouse_id').val(id);
            $('#warehouse').val(warehouse);
            $('#address').val(address);
    
            $('#modal-warehouse-label').html(`Edit Warehouse`);
            $('#modal-warehouse').modal('show');
        })

        const errorValidation = (err) => {
            let alert = '';
            const message = err;
            for (const key in message){
                    alert += `<div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <strong>Sory !</strong> ${message[key][0]}.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>`
                };
            $('#alert').html(alert);
        }

        const manage = (url, data, method) => {
            fetch(url, {
                method: method,
                headers: {
                    'Content-Type': 'application/json',
                    "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
                },
                credentials: "same-origin",
                body: JSON.stringify(data)
            })
            .then(res => res.json())
            .then((result) => {
                result.metadata.code === 200 && swal('Hey', result.metadata.message, 'success');
                result.metadata.code === 201 && errorValidation(result.metadata.message);
                result.metadata.code === 200 && $('#data-warehouse').DataTable().ajax.reload();
                result.metadata.code === 200 && $('#modal-warehouse').modal('hide');
            }).catch((err) => {
                console.log(err)
            });
        }

        $('#data-warehouse').on('click', '#btn-delete', function(){
            const id = $(this).data('id');
            swal({
                title: "Are you sure?",
                text: "Delete this data",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
            .then((willDelete) => {
                if (willDelete) {
                    const url = `{{ url('warehouse') }}/${id}`;
                    manage(url,{},'DELETE');
                    swal("hey! Your data has been deleted!",'','success');
                } else {
                    swal("Cancel");
                }
            });
        })

        $('#btn-save').on('click', function(){
            warehouse = $('#warehouse').val();
            address = $('#address').val();
            warehouse_id = $('#warehouse_id').val();

            let data = {
                warehouse: warehouse,
                warehouse_id: warehouse_id,
                address: address,
                store_id: store_id
            };
            let url = `{{ url('warehouse')}}`;
            if(warehouse_id !== ''){
                url = `{{url('warehouse')}}/${warehouse_id}`;
                manage(url, data, 'PUT');
                return;
            }
            manage(url, data, 'POST')
        })
    });
</script>