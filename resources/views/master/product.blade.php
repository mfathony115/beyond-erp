@extends('layouts.layout')
@section('content')
<style>
    /* Extra small devices (portrait phones, less than 576px) */
    @media (min-width:400px) and (max-width: 575.98px) {
        .qr-reader{
            width: 100%;
        }
    }

    /* Small devices (landscape phones, 576px and up) */
    /* @media (min-width: 576px) and (max-width: 767.98px) { ... } */

    /* Medium devices (tablets, 768px and up) */
    /* @media (min-width: 768px) and (max-width: 991.98px) { ... } */

    /* Large devices (desktops, 992px and up) */
    @media (min-width: 992px) and (max-width: 1199.98px) {
        .qr-reader{
            width: 300px;
        }
    }

    /* Extra large devices (large desktops, 1200px and up) */
    @media (min-width: 1200px) and (max-width: 1300px) {
        .qr-reader{
            left: 25%;
            width: 500px;
        }
    }
    .dropdown.bootstrap-select.form-control{
        border: 1px solid #d1d3e2;
    }
</style>
<div class="container-fluid">   
    <!-- Modal -->
    <div class="modal fade" id="modal-unit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Product Modal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="product_form" name="product_form">
                        <input type="hidden" class="reset" id="product_id" name="product_id">
                        <div class="row">
                            <div class="col-md-12" id="alert">
                            </div>
                            @if (Auth::user()->store_id === 1)
                                <div class="mb-3 col-md-6">
                                    <label for="store_id" class="col-form-label">Store:</label>
                                    <input type="hidden" name="store" id="store">
                                    <select name="store_id" id="store_id" class="form-control reset">
                                        <option value=""></option>
                                        @foreach ($stores as $store)
                                            <option value="{{ $store->id }}">{{ $store->store }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                            <div class="mb-3 col-md-6">
                                <label for="unit" class="col-form-label">Product Code:</label>
                                <input type="text" id="product_code" name="product_code" class="form-control reset">
                            </div>
                            <div class="mb-3 col-md-6">
                                <label for="unit" class="col-form-label">Product Name:</label>
                                <input type="text" id="product_name" name="product_name" class="form-control reset">
                            </div>
                            <div class="mb-3 col-md-6">
                                <label for="unit_id" class="col-form-label">Unit:</label>
                                <input type="hidden" name="unit" id="unit">
                                <select name="unit_id" id="unit_id" class="form-control reset">
                                    <option value=""></option>
                                    @foreach ($unit as $unit)
                                        <option value="{{ $unit->id }}">{{ $unit->unit }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb-3 col-md-6">
                                <label for="category_id" class="col-form-label">Category:</label>
                                <input type="hidden" name="category" id="category">
                                <select name="category_id" id="category_id" class="form-control reset">
                                    <option value=""></option>
                                    @foreach ($category as $category)
                                        <option value="{{ $category->id }}">{{ $category->category }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb3 col-md-6">
                                <label for="merk_id" class="col-form-label">Merk:</label>
                                <input type="hidden" id="merk" name="merk">
                                <select id="merk_id" name="merk_id" class="selectpicker form-control" data-live-search="true">
                                    @foreach ($merk as $merk)
                                        <option value="{{ $merk->id }}">{{ $merk->merk }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="mb3 col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="purchase_price" class="col-form-label">Purchase Price:</label>
                                        <input type="text" name="purchase_price" id="purchase_price" class="form-control">         
                                    </div>
                                    <div class="col-md-12">
                                        <label for="bkp" class="col-form-label">BKP:</label>
                                        <select name="bkp" id="bkp" class="form-control">
                                            <option value="0">BKP</option>
                                            <option value="1">Non BKP</option>
                                            <option value="2">Tanpa Pajak</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="mb3 col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="selling_price" class="col-form-label">Selling Price:</label>
                                        <input type="text" name="selling_price" id="selling_price" class="form-control">        
                                    </div>
                                    <div class="col-md-4">
                                        <label for="ppn" class="col-form-label">Ppn:</label>
                                        <input type="text" name="ppn" id="ppn" class="form-control">        
                                    </div>
                                    <div class="col-md-4">
                                        <label for="pph" class="col-form-label">Pph:</label>
                                        <input type="text" name="pph" id="pph" class="form-control">        
                                    </div>
                                    <div class="col-md-4">
                                        <label for="dpp" class="col-form-label">Dpp:</label>
                                        <input type="text" name="dpp" id="dpp" class="form-control">        
                                    </div>
                                </div>
                            </div>
                            <div class="mb3 col-md-6">
                                <label for="description" class="col-form-label">Description:</label>
                                <textarea class="form-control" name="description" id="description" cols="30" rows="4"></textarea>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="min_stock" class="col-form-label">Min Stock:</label>
                                        <input type="text" name="min_stock" id="min_stock" class="form-control">        
                                    </div>
                                    <div class="col-md-6">
                                        <label for="max_stock" class="col-form-label">Max Stock:</label>
                                        <input type="text" name="max_stock" id="max_stock" class="form-control">        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="btn-save" type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <div class="card shadow mb-4" id="table-container">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Product
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-success btn-sm ml-2" data-toggle="modal" id="btn-add" data-target="#modal-unit">
                    <i class="fas fa-plus"></i>
                </button>
            </h6>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                @if (Auth::user()->store_id === 1)
                    <div class="mb-3 ui-widget">
                        <label for="unit" class="col-form-label">Store:</label>
                        <select name="select_store" id="select_store" class="form-control reset">
                            @foreach ($stores as $store)
                                <option value="{{ $store->id }}">{{ $store->store }}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
                @if(Auth::user()->store_id !== 1)
                    <input name="select_store" id="select_store" type="text" hidden value="{{Auth::user()->store_id}}">
                @endif
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="data-product" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>Product Code</th>
                            <th>Product Name</th>
                            <th>Unit</th>
                            <th>Category</th>
                            <th>Merk</th>
                            <th>Description</th>
                            <th>Bkp</th>
                            <th>Purchase Price</th>
                            <th>Ppn</th>
                            <th>Pph</th>
                            <th>Dpp</th>
                            <th>Selling Price</th>
                            <th>Min Stock</th>
                            <th>Max Stock</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Product Code</th>
                            <th>Product Name</th>
                            <th>Unit</th>
                            <th>Category</th>
                            <th>Merk</th>
                            <th>Description</th>
                            <th>Bkp</th>
                            <th>Purchase Price</th>
                            <th>Ppn</th>
                            <th>Pph</th>
                            <th>Dpp</th>
                            <th>Selling Price</th>
                            <th>Min Stock</th>
                            <th>Max Stock</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="card shadow mb-4" id="barcode-container" hidden>
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">
                <button type="button" id="btn-back" class="btn btn-success btn-circle btn-sm ml-2">
                    <i class="fas fa-arrow-left"></i>
                </button>
                Product Barcode
            </h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 text-center">
                    <div class="mb-3 text-center">
                        <div id="qr-reader" class="qr-reader"></div>
                        <div id="qr-reader-results" class="qr-reader-result"></div>
                    </div>  
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <textarea name="barcode_text" id="barcode_text" class="form-control" cols="30" rows="4"></textarea>
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
<script src="{{ asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('js/sweetalert.min.js')}}"></script>
<script src="{{ asset('vendor/html5-qrcode/js/html5-qrcode.min.js')}}" type="text/javascript"></script>
<script>
    $(function () {
        let resultContainer = document.getElementById('qr-reader-results');
        let lastResult, countResults = 0;
        let barcode_text = document.getElementById('barcode_text');
        const config = { fps: 10, qrbox: 250 };
        const html5QrcodeScanner = new Html5QrcodeScanner("qr-reader", config);
        const status = `{{ Auth::user()->store_id }}`;

        function onScanSuccess(decodedText, decodedResult) {
            // if (decodedText !== lastResult) {
            //     ++countResults;
            //     lastResult = decodedText;
                // Handle on success condition with the decoded message.
                console.log(`Scan result ${decodedText}`, decodedResult);
                resultContainer.innerHTML = `Scan result ${decodedText}`;
                barcode_text.innerHTML = decodedText;
            // }

        }

        function onScanFailure(error) {
            // handle scan failure, usually better to ignore and keep scanning.
            // for example:
            console.warn(`Code scan error = ${error}`);
        }

        const getListDropdown = async (url, method) => {
            const response = await fetch(url, {
                method: method,
                headers: {
                    'Content-Type': 'application/json',
                    "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
                },
                credentials: "same-origin"
            });
            return response;
        }    

        $('#data-product').on('click', '#btn-barcode', function(){
            html5QrcodeScanner.render(onScanSuccess, onScanFailure);
            $('#table-container').prop('hidden', true);
            $('#barcode-container').prop('hidden', false);
        })

        $('#btn-back').on('click', function(){
            $('#barcode-container').prop('hidden', true);
            $('#table-container').prop('hidden', false);
        })

        const errorValidation = (err) => {
            let alert = '';
            const message = err;
            for (const key in message){
                    alert += `<div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <strong>Sory !</strong> ${message[key][0]}.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>`
                };
            $('#alert').html(alert);
        }

        const getData = (status) => {
            $('#data-product').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                url: `{{ url('product/data') }}/${status}`,
                type: "POST",
                headers: {
                    "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
                },
                credentials: "same-origin"
                },
                columns: [
                    { data: null,
                        render: function(data, type, row, meta){
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: "action"},
                    { data: "product_code" },
                    { data: "product_name"},
                    { data: "unit" },
                    { data: "category" },
                    { data: "merk" },
                    { data: "description" },
                    { data: "bkp" },
                    { data: "purchase_price" },
                    { data: "ppn" },
                    { data: "pph" },
                    { data: "dpp" },
                    { data: "selling_price" },
                    { data: "min_stock" },
                    { data: "max_stock" },
                ]
            });
        }

        getData(status);

        const manage = (url, data, method) => {
            fetch(url, {
                method: method,
                headers: {
                    "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
                },
                credentials: "same-origin",
                body: data
            })
            .then(res => res.json())
            .then((result) => {
                result.metadata.code === 200 && swal('Hey', result.metadata.message, 'success');
                result.metadata.code === 201 && errorValidation(result.metadata.message);
                result.metadata.code === 200 && $('#data-product').DataTable().ajax.reload();
                result.metadata.code === 200 && $('#modal-unit').modal('hide');
            }).catch((err) => {
                console.log(err)
            });
        }

        $('#purchase_price').on('keyup', function(){
            let purchase_price = $(this).val();
            $('#purchase_price').val(numeral(purchase_price).format());
            get_price()
        });

        $('#bkp').on('change', function(){
            get_price();
        });

        const get_price = () => {
            let purchase_price = parseInt(numeral($('#purchase_price').val()).value());
            let status_bkp = $('#bkp').val();
            let dpp, ppn, pph = 0;
            if(status_bkp === '0' ){
                dpp = (purchase_price*100/110);
                ppn = (dpp*0.1);
                pph = (purchase_price*2.5/100)
            } else if(status_bkp === '1'){
                dpp = 0;
                ppn = (dpp*10/100);
                pph = (purchase_price*2.5/100);
            } else {
                dpp = 0;
                ppn = 0;
                pph = 0;
            }

            $('#dpp').val(numeral(dpp).format());
            $('#ppn').val(numeral(ppn).format());
            $('#pph').val(numeral(pph).format());
        }


        $('#unit_id').on('change', function(){
            const unit = $('#unit_id option:selected').text();
            $('#unit').val(unit);
        })

        $('#category_id').on('change', function(){
            const category = $('#category_id option:selected').text();
            $('#category').val(category);
        })

        $('#merk_id').on('change', function(){
            const merk = $('#merk_id option:selected').text();
            $('#merk').val(merk);
        })

        $('#btn-add').on('click', function(){
            $('.reset').val('');
        })

        $('#data-product').on('click', '#btn-edit', function() {
            const id = $(this).data('id');
            const url = `{{ url('product')}}/${id}`
            fetchGet(url)
             .then(res => {
                if(res.metadata.code === 200){
                    const data = res.data;
                    $('#product_id').val(data.id)
                    $('#store_id').val(data.store_id);
                    $('#product_code').val(data.product_code);
                    $('#product_name').val(data.product_name);
                    $('#unit').val(data.unit.unit);
                    $('#unit_id').val(data.unit_id);
                    $('#category').val(data.category.category);
                    $('#category_id').val(data.category_id);
                    $('#merk').val(data.merk.merk);
                    $('#merk_id').val(data.merk_id);
                    $('#merk_id').selectpicker('refresh');
                    $('#purchase_price').val(data.purchase_price);
                    $('#bkp').val(data.bkp);
                    $('#selling_price').val(data.selling_price);
                    $('#ppn').val(data.ppn);
                    $('#pph').val(data.pph);
                    $('#dpp').val(data.dpp);
                    $('#description').val(data.description);
                    $('#min_stock').val(data.min_stock);
                    $('#max_stock').val(data.max_stock);

                }
             })
             .catch(err => console.log(err));
            $('#modal-unit').modal('show');

        })

        $('#data-product').on('click', '#btn-delete', function(){
            const id = $(this).data('id');
            swal({
                title: "Are you sure?",
                text: "Delete this data",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
            .then((willDelete) => {
                if (willDelete) {
                    const url = `{{ url('unit') }}/${id}`;
                    manage(url,{},'DELETE');
                    swal("Poof! Your data has been deleted!",'','success');
                } else {
                    swal("Cancel");
                }
            });
        })

        $('#btn-save').on('click', function(){
            let product_form = document.getElementById('product_form');
            let formData = new FormData(product_form);
            let url = `{{url('product')}}`
            let product_id = $('#product_id').val();
            if(product_id !== ''){
                url = `{{url('product')}}/${product_id}`;
                manage(url, formData, 'POST');
                return;
            }
            manage(url, formData, 'POST')
        });
        
    });
</script>