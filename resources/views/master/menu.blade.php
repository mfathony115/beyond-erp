@extends('layouts.layout')
@section('content')
<div class="container-fluid">
    {{-- modal menu --}}
    <div class="modal fade" id="modal-menu" tabindex="-1" aria-labelledby="modal-menu-label" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-menu-label">New Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" id="alert">
                </div>
                <div class="col-md-12">
                    <form id="form-menu">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <input type="hidden" class="reset" name="menu_id" id="menu_id">
                                <input type="hidden" class="reset" name="parent" id="parent" value="0">
                                <label for="menu" class="col-form-label">Menu :</label>
                                <input type="text" class="form-control reset" name="menu" required id="menu">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="url" class="col-form-label">Url :</label>
                                <input type="text" class="form-control reset" name="url" required id="url">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="icon" class="col-form-label">Icon :</label>
                                <input type="text" class="form-control reset" name="icon" required id="icon">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="btn-save" class="btn btn-primary">Save</button>
            </div>
            </div>
        </div>
    </div>
    {{-- end modal menu --}}
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">
                Table Menu 
                <button data-toggle="modal" id="btn-tambah" data-target="#modal-menu" class="btn btn-circle btn-primary btn-sm ml-2">
                    <i class="fas fa-plus"></i>
                </button>
            </h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="data-menu" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Menu</th>
                            <th>Url</th>
                            <th>Icon</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Menu</th>
                            <th>Url</th>
                            <th>Icon</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection
<script src="{{ asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('js/sweetalert.min.js')}}"></script>

<script>
    $(function () {
        const status = `{{ Auth::user()->store_id }}`;
        let menu_id = $('#menu_id').val();
        let menu = $('#menu').val();
        let url_menu = $('#url').val();
        let icon = $('#icon').val();
        let parent = $('#parent').val();

        $('#data-menu').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
               url: `{{ url('menu/data') }}/${status}`,
               type: "POST",
               headers: {
                "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
               },
               credentials: "same-origin"
            },
            columns: [
                { data: 'DT_RowIndex'},
                { data: "menu" },
                { data: "url"},
                { data: "icon"},
                {
                    data: 'action', 
                    name: 'action', 
                    orderable: true, 
                    searchable: true
                }
            ]
        });

        $('#btn-tambah').on('click', function(){
            $('#modal-menu-label').html(`New Menu`);
            $('.reset').val('');
        });

        $('#data-menu').on('click', '#btn-edit', function(){
            const id = $(this).data('id');
            const menu = $(this).data('menu');
            const parent = $(this).data('parent');
            const url_menu = $(this).data('url');
            const icon = $(this).data('icon');

            $('#menu_id').val(id);
            $('#menu').val(menu);
            $('#url').val(url_menu);
            $('#icon').val(icon);
            $('#parent').val(parent);
            $('#modal-menu-label').html(`Edit Menu`);
            $('#modal-menu').modal('show');
        })

        $('#data-menu').on('click', '#btn-child', function(){
            $('.reset').val('');
            const id = $(this).data('id');
            const menu = $(this).data('menu');
            $('#parent').val(id);
            $('#modal-menu-label').html(`Penambahan submenu pada menu <b>${menu}</b>`);
            $('#modal-menu').modal('show');
        })

        const errorValidation = (err) => {
            let alert = '';
            const message = err;
            for (const key in message){
                    alert += `<div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <strong>Sory !</strong> ${message[key][0]}.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>`
                };
            $('#alert').html(alert);
        }

        const manage = (url, data, method) => {
            fetch(url, {
                method: method,
                headers: {
                    'Content-Type': 'application/json',
                    "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
                },
                credentials: "same-origin",
                body: JSON.stringify(data)
            })
            .then(res => res.json())
            .then((result) => {
                result.metadata.code === 200 && swal('Hey', result.metadata.message, 'success');
                result.metadata.code === 201 && errorValidation(result.metadata.message);
                result.metadata.code === 200 && $('#data-menu').DataTable().ajax.reload();
                result.metadata.code === 200 && $('#modal-menu').modal('hide');
            }).catch((err) => {
                console.log(err)
            });
        }

        $('#btn-save').on('click', function(){
            const store_id = `{{Auth::user()->store_id}}`
            menu = $('#menu').val();
            url_menu = $('#url').val();
            icon = $('#icon').val();
            parent = $('#parent').val();
            menu_id = $('#menu_id').val();

            let data = {
                store_id: store_id,
                parent: parent,
                menu: menu,
                url_menu: url_menu,
                icon: icon
            };
            let url = `{{ url('menu')}}`;
            if(menu_id !== ''){
                url = `{{url('menu')}}/${menu_id}`;
                manage(url, data, 'PUT');
                return;
            }
            manage(url, data, 'POST')
        });

        $('#data-menu').on('click', '#btn-delete', function(){
            const id = $(this).data('id');
            swal({
                title: "Are you sure?",
                text: "Delete this data",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
            .then((willDelete) => {
                if (willDelete) {
                    const url = `{{ url('menu') }}/${id}`;
                    manage(url,{},'DELETE');
                    swal("Poof! Your data has been deleted!",'','success');
                } else {
                    swal("Cancel");
                }
            });
        });
    });
</script>