@extends('layouts.layout')
@section('content')
<style>
     .dropdown.bootstrap-select.form-control{
        border: 1px solid #d1d3e2;
    }
</style>
<div class="container-fluid">
    {{-- modal user --}}
    <div class="modal fade" id="modal-user" tabindex="-1" aria-labelledby="modal-user-label" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-user-label">New User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12" id="alert">
                </div>
                <div class="col-md-12">
                    <form id="form-user">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="ktp" class="col-form-label">KTP :</label>
                                <input type="hidden" id="user_id" name="user_id" class="reset">
                                <input type="text" class="form-control reset" required id="ktp" name="ktp">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="username" class="col-form-label">Username :</label>
                                <input type="text" class="form-control reset" required id="username" name="username">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="name" class="col-form-label">Name :</label>
                                <input type="text" class="form-control reset" required id="name" name="name">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="birth" class="col-form-label">Birth :</label>
                                <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                                    <input type="text" id="birth" name="birth" class="form-control datetimepicker-input reset" data-target="#datetimepicker1"/>
                                    <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="store" class="col-form-label">Store :</label>
                                <input type="hidden" class="form-control reset" required id="store" name="store">
                                <select id="store_id" name="store_id" class="selectpicker form-control reset" data-live-search="true">
                                    <option value="" selected>Choose Store</option>
                                    @foreach ($stores as $store)
                                        <option value="{{ $store->id }}">{{ $store->store }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="position" class="col-form-label">Position :</label>
                                <input type="hidden" class="form-control reset" required id="position" name="position">
                                <select id="position_id" name="position_id" class="selectpicker form-control reset" data-live-search="true">
                                    <option value="" selected>Choose Position</option>
                                    @foreach ($positions as $positions)
                                        <option value="{{ $positions->id }}">{{ $positions->position }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="email" class="col-form-label">Email :</label>
                                <input type="email" class="form-control reset" required id="email" name="email">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="address" class="col-form-label">Address:</label>
                                <textarea class="form-control reset" required id="address" name="address"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="btn-save" class="btn btn-primary">Save</button>
            </div>
            </div>
        </div>
    </div>
    {{-- end modal user --}}
    <!-- DataTales -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">
                Table User 
                <button id="btn-modal-add" data-toggle="modal" data-target="#modal-user" class="btn btn-circle btn-primary btn-sm ml-2">
                    <i class="fas fa-plus"></i>
                </button>
            </h6>
        </div>
        <div class="card-body">
            <div class="col-md-12">
                @if (Auth::user()->store_id === 1)
                    <div class="mb-3 ui-widget">
                        <label for="unit" class="col-form-label">Store:</label>
                        <select name="select_store" id="select_store" class="form-control reset">
                            @foreach ($stores as $store)
                                <option value="{{ $store->id }}">{{ $store->store }}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
                @if(Auth::user()->store_id !== 1)
                    <input name="select_store" id="select_store" type="text" hidden value="{{Auth::user()->store_id}}">
                @endif
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="data-user" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>Ktp</th>
                            <th>Username</th>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Birth</th>
                            <th>Email</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>Ktp</th>
                            <th>Username</th>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Birth</th>
                            <th>Email</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection
<script src="{{ asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('js/sweetalert.min.js')}}"></script>

<script>
    $(function () {
        const store_id = `{{ Auth::user()->store_id }}`;
        const store = `{{ Auth::user()->store }}`;
        const user = `{{ Auth::user()->username }}`;

        $('#datetimepicker1').datetimepicker({
            format: 'DD-MM-YYYY'
        });

        $('#btn-modal-add').on('click', function(e){
            $('#ktp').prop('readonly', false);
            $('#username').prop('readonly', false);
            $('.reset').val('');
        })

        const showData = (store_id) => {
            $('#data-user').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                ajax: {
                url: `{{ url('user/data') }}/${store_id}`,
                type: "GET",
                headers: {
                    "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
                },
                credentials: "same-origin"
                },
                columns: [
                    { data: 'DT_RowIndex'},
                    {
                        data: 'action', 
                        name: 'action', 
                        orderable: true, 
                        searchable: true
                    },
                    { data: "ktp" },
                    { data: "username" },
                    { data: "name" },
                    { data: "position" },
                    { data: "birth" },
                    { data: "email" }
                ]
            });
        }
        showData(store_id);

        $('#select_store').on('change', function(){
            const storeid = $(this).val();
            showData(storeid);
        })

        $('#store_id').on('change', function(){
            const store = $('#store_id option:selected').text();
            $('#store').val(store);
        });

        $('#position_id').on('change', function(){
            const position = $('#position_id option:selected').text();
            $('#position').val(position);
        });

        $('#data-user').on('click', '#btn-edit', function(){
            const id = $(this).data('id');
            $('#ktp').prop('readonly', true);
            $('#username').prop('readonly', true);
            $('#user_id').val(id);
            const url = `{{ url('user') }}/${id}`;
            fetchGet(url)
            .then(res => {
                if(res.metadata.code === 200){
                    const data = res.data;
                    const birth = moment(data.birth, 'YYYY-MM-DD').format('DD-MM-YYYY');
                    $('#ktp').val(data.ktp);
                    $('#username').val(data.username);
                    $('#name').val(data.name);
                    $('#birth').val(birth);
                    $('#store').val(data.store);
                    $('#store_id').val(data.store_id);
                    $('#store_id').selectpicker('refresh');
                    $('#position').val(data.position);
                    $('#position_id').val(data.position_id);
                    $('#position_id').selectpicker('refresh');
                    $('#email').val(data.email);
                    $('#address').val(data.address);
                }
            })
            .catch(err => console.log(err));
            $('#modal-user').modal('show');
        });

        const errorValidation = (err) => {
            let alert = '';
            const message = err;
            for (const key in message){
                    alert += `<div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <strong>Sory !</strong> ${message[key][0]}.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>`
                };
            $('#alert').html(alert);
        }

        const manage = async (url, data, method) => {
            await fetch(url, {
                method: method,
                headers: {
                    "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
                },
                credentials: "same-origin",
                body: data
            })
            .then(res => res.json())
            .then((result) => {
                result.metadata.code === 200 && swal('Hey', result.metadata.message, 'success');
                result.metadata.code === 201 && errorValidation(result.metadata.message);
                result.metadata.code === 200 && $('#data-user').DataTable().ajax.reload();
                result.metadata.code === 200 && $('#modal-user').modal('hide');
            }).catch((err) => {
                console.log(err)
            });
        }

        $('#data-user').on('click', '#btn-delete', function(){
            const id = $(this).data('id');
            swal({
                title: "Are you sure?",
                text: "Delete this data",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
            .then((willDelete) => {
                if (willDelete) {
                    const url = `{{ url('user') }}/${id}`;
                    manage(url,{},'DELETE');
                } else {
                    swal("Cancel");
                }
            });
        })

        $('#btn-save').on('click', function(){
            const formUser = document.getElementById("form-user");
            const data = new FormData(formUser);
            let url = `{{ url('user')}}`;
            const user_id = $('#user_id').val();
            if(user_id !== ''){
                url = `{{ url('user/update/')}}/${user_id}`;
                manage(url, data, 'POST');
                return;
            }
            manage(url, data, 'POST');
        })
    });
</script>