@extends('layouts.layout')
@section('content')
<div class="container-fluid">   
    <!-- Modal -->
    <div class="modal fade" id="modal-unit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Unit Modal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        @if (Auth::user()->store_id === 1)
                            <div class="mb-3 ui-widget">
                                <label for="unit" class="col-form-label">Store:</label>
                                <select name="store" id="store" class="form-control reset">
                                    @foreach ($stores as $store)
                                        <option value="{{ $store->id }}">{{ $store->store }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        <div class="mb-3 ui-widget">
                            <input type="hidden" name="unit_id" id="unit_id" class="reset">
                            <label for="unit" class="col-form-label">Unit Code:</label>
                            <input type="text" id="unit_code" name="unit_code" class="form-control reset">
                        </div>
                        <div class="mb-3 ui-widget">
                          <label for="unit" class="col-form-label">Unit:</label>
                          <input type="text" id="unit" name="unit" class="form-control reset">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="btn-save" type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Unit
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-success btn-sm ml-2" data-toggle="modal" id="btn-add" data-target="#modal-unit">
                    <i class="fas fa-plus"></i>
                </button>
            </h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="data-unit" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Unit Code</th>
                            <th>Unit</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Unit Code</th>
                            <th>Unit</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection
<script src="{{ asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('js/sweetalert.min.js')}}"></script>

<script>
    $(function () {
        const status = `{{ Auth::user()->store_id === 1 ? 'all': 'byStore' }}`;
        const getData = (status) => {
            $('#data-unit').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                url: `{{ url('unit/data') }}/${status}`,
                type: "POST",
                headers: {
                    "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
                },
                credentials: "same-origin"
                },
                columns: [
                    { data: null,
                        render: function(data, type, row, meta){
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: "unit_code" },
                    { data: "unit" },
                    { data: "action"}
                ]
            });
        }

        getData(status)
        const manage = (url, data, method) => {
            fetch(url, {
                method: method,
                headers: {
                    'Content-Type': 'application/json',
                    "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
                },
                credentials: "same-origin",
                body: JSON.stringify(data)
            })
            .then(res => res.json())
            .then((result) => {
                result.metadata.code === 200 && swal('Hey', result.metadata.message, 'success');
                $('#data-unit').DataTable().ajax.reload();
                $('#modal-unit').modal('hide');
            }).catch((err) => {
                console.log(err)
            });
        }

        $('#btn-add').on('click', function(){
            $('.reset').val('');
        })

        $('#data-unit').on('click', '#btn-edit', function() {
            const id = $(this).data('id');
            const unit = $(this).data('unit');
            const unit_code = $(this).data('unit_code');
            $('#unit_id').val(id);
            $('#unit_code').val(unit_code);
            $('#unit').val(unit);
            $('#modal-unit').modal('show');
        })

        $('#data-unit').on('click', '#btn-delete', function(){
            const id = $(this).data('id');
            swal({
                title: "Are you sure?",
                text: "Delete this data",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
            .then((willDelete) => {
                if (willDelete) {
                    const url = `{{ url('unit') }}/${id}`;
                    manage(url,{},'DELETE');
                    swal("Poof! Your data has been deleted!",'','success');
                } else {
                    swal("Cancel");
                }
            });
        })

        $('#btn-save').on('click', function(){
            const store = $('#store').val();
            const unit_code = $('#unit_code').val();
            const unit = $('#unit').val();
            const unit_id = $('#unit_id').val();
            let data = {
                store_id: store,
                unit_code: unit_code,
                unit: unit
            };
            let url = `{{url('unit')}}`;
            if(unit_id !== ''){
                data = {
                    unit: unit
                };
                url = `{{url('unit')}}/${unit_id}`
                manage(url, data, 'PUT');
                return;
            }
            
            manage(url, data, 'POST');
        })
    });
</script>