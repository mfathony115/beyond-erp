@extends('layouts.layout')
@section('content')
<div class="container-fluid">   
    <!-- Modal -->
    <div class="modal fade" id="modal-category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Category Modal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        @if (Auth::user()->store_id === 1)
                            <div class="mb-3 ui-widget">
                                <label for="unit" class="col-form-label">Store:</label>
                                <select name="store" id="store" class="form-control reset">
                                    @foreach ($stores as $store)
                                        <option value="{{ $store->id }}">{{ $store->store }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        <div class="mb-3 ui-widget">
                            <input type="hidden" name="category_id" id="category_id" class="reset">
                            <label for="unit" class="col-form-label">Category Code:</label>
                            <input type="text" id="category_code" name="category_code" class="form-control reset">
                        </div>
                        <div class="mb-3 ui-widget">
                          <label for="unit" class="col-form-label">Category:</label>
                          <input type="text" id="category" name="category" class="form-control reset">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="btn-save" type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Category
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-success btn-sm ml-2" data-toggle="modal" id="btn-add" data-target="#modal-category">
                    <i class="fas fa-plus"></i>
                </button>
            </h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="data-category" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Category Code</th>
                            <th>Category</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Category Code</th>
                            <th>Category</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection
<script src="{{ asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('js/sweetalert.min.js')}}"></script>

<script>
    $(function () {
        const status = `{{ Auth::user()->store_id === 1 ? 'all': 'byStore' }}`;
        const getData = (status) => {
            $('#data-category').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: `{{ url('category/data') }}/${status}`,
                    type: "POST",
                    headers: {
                        "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
                    },
                    credentials: "same-origin"
                },
                columns: [
                    { data: null,
                        render: function(data, type, row, meta){
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    { data: "category_code" },
                    { data: "category" },
                    { data: "action"}
                ]
            });
        }

        getData(status)
        const manage = (url, data, method) => {
            fetch(url, {
                method: method,
                headers: {
                    'Content-Type': 'application/json',
                    "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
                },
                credentials: "same-origin",
                body: JSON.stringify(data)
            })
            .then(res => res.json())
            .then((result) => {
                result.metadata.code === 200 && swal('Hey', result.metadata.message, 'success');
                $('#data-category').DataTable().ajax.reload();
                $('#modal-category').modal('hide');
            }).catch((err) => {
                console.log(err)
            });
        }

        $('#btn-add').on('click', function(){
            $('.reset').val('');
        })

        $('#data-category').on('click', '#btn-edit', function() {
            const id = $(this).data('id');
            const position = $(this).data('category');
            const position_code = $(this).data('category_code');
            $('#category_id').val(id);
            $('#category_code').val(position_code);
            $('#category').val(position);
            $('#modal-category').modal('show');
        })

        $('#data-category').on('click', '#btn-delete', function(){
            const id = $(this).data('id');
            swal({
                title: "Are you sure?",
                text: "Delete this data",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
            .then((willDelete) => {
                if (willDelete) {
                    const url = `{{ url('category') }}/${id}`;
                    manage(url,{},'DELETE');
                    swal("Poof! Your data has been deleted!",'','success');
                } else {
                    swal("Cancel");
                }
            });
        })

        $('#btn-save').on('click', function(){
            const store = $('#store').val();
            const category_code = $('#category_code').val();
            const category = $('#category').val();
            const category_id = $('#category_id').val();
            let data = {
                store_id: store,
                category_code: category_code,
                category: category
            };
            let url = `{{url('category')}}`;
            if(category_id !== ''){
                data = {
                    category: category
                };
                url = `{{url('category')}}/${category_id}`
                manage(url, data, 'PUT');
                return;
            }
            
            manage(url, data, 'POST');
        })
    });
</script>