@extends('layouts.layout')
@section('content')
<div class="container-fluid">   
    <!-- Modal -->
    <div class="modal fade" id="modal-supplier" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Vendor Modal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12" id="alert"></div>
                    <form>
                        @if (Auth::user()->store_id === 1)
                            <div class="mb-3 ui-widget" hidden>
                                <label for="unit" class="col-form-label">Store:</label>
                                <select name="store" id="store" class="form-control reset">
                                    @foreach ($stores as $store)
                                        <option value="{{ $store->id }}">{{ $store->store }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-6 mb-3 ui-widget">
                                <input type="hidden" name="vendor_id" id="vendor_id" class="reset">
                                <label for="vendor" class="col-form-label">Vendor:</label>
                                <input type="text" id="vendor" name="vendor" class="form-control reset" placeholder="Vendor Name ...">
                            </div>
                            <div class="col-md-6 mb-3 ui-widget">
                              <label for="email" class="col-form-label">Email:</label>
                              <input type="text" id="email" name="email" class="form-control reset" placeholder="Email ...">
                            </div>
                            <div class="col-md-6 mb-3 ui-widget">
                                <label for="npwp" class="col-form-label">Npwp:</label>
                                <input type="text" id="npwp" name="npwp" class="form-control reset" placeholder="Please entry your NPWP if you have ...">
                            </div>
                            <div class="col-md-6 mb-3 ui-widget">
                              <label for="telp" class="col-form-label">Telp:</label>
                              <input type="text" id="telp" name="telp" class="form-control reset" placeholder="Telp ...">
                            </div>
                            <div class="col-md-12 mb-3 ui-widget">
                              <label for="telp" class="col-form-label">Tipe:</label>
                              <input type="hidden" name="tipe" class="reset" id="tipe">
                              <div class="row">
                                <div class="col-md-3 mb-3 ui-widget">
                                    <div class="card border-left-info shadow h-100 card-tipe" id="card-perusahaan" data-tipe="perusahaan">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Perusahaan</div>  
                                                </div>
                                                <div class="col-auto">
                                                    <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3 ui-widget">
                                    <div class="card border-left-info shadow h-100 card-tipe" id="card-perorangan" data-tipe="perorangan">
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Perorangan</div>  
                                                </div>
                                                <div class="col-auto">
                                                    <i class="fas fa-user fa-2x text-gray-300"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-8 mb-3 ui-widget">
                                <label for="telp" class="col-form-label">Address:</label>
                                <textarea class="form-control reset" name="address" id="address"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="btn-save" type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Vendor
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-success btn-sm ml-2" data-toggle="modal" id="btn-add" data-target="#modal-supplier">
                    <i class="fas fa-plus"></i>
                </button>
            </h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="data-vendor" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Telp</th>
                            <th>Address</th>
                            <th>Npwp</th>
                            <th>Tipe</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Telp</th>
                            <th>Address</th>
                            <th>Npwp</th>
                            <th>Tipe</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection
<script src="{{ asset('vendor/jquery/jquery.min.js')}}"></script>
<script src="{{ asset('js/sweetalert.min.js')}}"></script>

<script>
    $(function () {
        const status = `{{ Auth::user()->store_id === 1 ? 'all': 'byStore' }}`;
        const store = `{{ Auth::user()->store_id }}`;
        const token = localStorage.getItem('token');

        const getData = (status) => {
            $('#data-vendor').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                url: `{{ url('supplier/data') }}/${status}`,
                type: "POST",
                headers: {
                    "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content'),
                    "Authorization": `Bearer ${token}`
                },
                credentials: "same-origin"
                },
                columns: [
                    { data: null,
                        render: function(data, type, row, meta){
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                      data: "id",
                      render: function(data, type, row, meta){
                        const button = `<button type="button" id="btn-edit" class="btn btn-circle btn-success btn-sm"
                        data-id="${data}" 
                        data-name="${row.name}"
                        data-email="${row.email}"
                        data-telp="${row.telp}"
                        data-address="${row.address}"
                        data-npwp="${row.npwp}"
                        data-tipe="${row.tipe}"
                        data-toggle="tooltip" data-placement="top" title="Edit"><i class="far fa-edit"></i></button> 
                        <button id="btn-delete" class="btn btn-danger btn-circle btn-sm"
                        data-id="${data}" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fas fa-trash"></i></button>`;
                        return button;
                      }
                    },
                    { data: "name" },
                    { data: "email" },
                    { data: "telp"},
                    { data: "address"},
                    { data: "npwp"},
                    { 
                      data: "tipe",
                      render: function(data, type, row, meta){
                        const title = data === 0 ? '<span class="badge badge-primary">Perusahaan</span>': '<span class="badge badge-success">Perorangan</span>';
                        return title;
                      }
                    }
                ]
            });
        }

        getData(status);

        $('#data-vendor').on('click', '#btn-edit', function() {
            const id = $(this).data('id');
            const name = $(this).data('name');
            const email = $(this).data('email');
            const npwp = $(this).data('npwp');
            const telp = $(this).data('telp');
            const tipe = $(this).data('tipe');
            const address = $(this).data('address');
            if(tipe === 1){
                $('#card-perusahaan').removeClass("bg-dark");
                $('#card-perorangan').addClass("bg-dark");
            } else {
                $('#card-perusahaan').addClass("bg-dark");
                $('#card-perorangan').removeClass("bg-dark");
            }
            $('#vendor_id').val(id);
            $('#vendor').val(name);
            $('#email').val(email);
            $('#npwp').val(npwp);
            $('#telp').val(telp);
            $('#tipe').val(tipe);
            $('#address').val(address);
            $('#modal-supplier').modal('show');
        })

        $('#data-vendor').on('click', '#btn-delete', function(){
            const id = $(this).data('id');
            swal({
                title: "Are you sure?",
                text: "Delete this data",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
            .then((willDelete) => {
                if (willDelete) {
                    const url = `{{ url('supplier') }}/${id}`;
                    manage(url,{},'DELETE');
                    swal("Poof! Your data has been deleted!",'','success');
                } else {
                    swal("Cancel");
                }
            });
        });

        const errorValidation = (err) => {
            let alert = '';
            const message = err;
            for (const key in message){
                    alert += `<div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <strong>Sory !</strong> ${message[key][0]}.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>`
                };
            $('#alert').html(alert);
        }

        const manage = (url, data, method) => {
            fetch(url, {
                method: method,
                headers: {
                    'Content-Type': 'application/json',
                    "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
                },
                credentials: "same-origin",
                body: JSON.stringify(data)
            })
            .then(res => res.json())
            .then((result) => {
                result.metadata.code === 200 && swal('Hey', result.metadata.message, 'success');
                result.metadata.code === 200 && $('#modal-supplier').modal('hide');
                result.metadata.code === 201 && errorValidation(result.metadata.message);
                $('#data-vendor').DataTable().ajax.reload();
            }).catch((err) => {
                console.log(err)
            });
        }

        $('.card-tipe').on('click', function(e){
            const tipe = $(this).data('tipe');
            if(tipe === 'perusahaan'){
                $('#card-perorangan').removeClass("bg-dark");
                $(this).addClass("bg-dark");
            } else {
                $('#card-perusahaan').removeClass("bg-dark");
                $(this).addClass("bg-dark");
            }
            $('#tipe').val(tipe);
        })

        $('#btn-add').on('click', function(){
            $('.reset').val('');
        })

        $('#btn-save').on('click', function(){
            const vendor_id = $('#vendor_id').val();
            const vendor = $('#vendor').val();
            const email = $('#email').val();
            const npwp = $('#npwp').val();
            const telp = $('#telp').val();
            const tipe = $('#tipe').val();
            const address = $('#address').val();
            let data = {
                store_id: store,
                name: vendor,
                email: email,
                npwp: npwp,
                telp: telp,
                tipe: tipe,
                address: address
            };
            let url = `{{ url('supplier') }}`;
            if(vendor_id !== ''){
                data = {
                    vendor_id: vendor_id,
                    store_id: store,
                    name: vendor,
                    email: email,
                    npwp: npwp,
                    telp: telp,
                    tipe: tipe,
                    address: address
                };
                url = `{{url('supplier')}}/${vendor_id}`
                manage(url, data, 'PUT');
                return;
            }
            
            manage(url, data, 'POST');
        })
    });
</script>