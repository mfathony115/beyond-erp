@extends('layouts.layout')
@section('content')
<style>
li {
    list-style: none;
}
</style>
<div class="container-fluid">   
    <!-- Modal -->
    <div class="modal fade" id="modal-position" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Positions Modal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12" id="alert"></div>
                    <form>
                        @if (Auth::user()->store_id === 1)
                            <div class="mb-3 ui-widget">
                                <label for="unit" class="col-form-label">Store:</label>
                                <select name="store" id="store" class="form-control reset">
                                    @foreach ($stores as $store)
                                        <option value="{{ $store->id }}">{{ $store->store }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        @if(Auth::user()->store_id !== 1)
                            <input name="store" id="store" type="text" hidden value="{{Auth::user()->store_id}}">
                        @endif
                        <div class="mb-3 ui-widget">
                            <input type="hidden" name="position_id" id="position_id" class="reset">
                            <label for="unit" class="col-form-label">Positions Code:</label>
                            <input type="text" id="position_code" name="position_code" class="form-control reset">
                        </div>
                        <div class="mb-3 ui-widget">
                          <label for="unit" class="col-form-label">Positions:</label>
                          <input type="text" id="position" name="position" class="form-control reset">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="btn-save" type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>
    <!-- modal menu -->
    <div class="modal fade" id="modal-role-menu" tabindex="-1" role="dialog" aria-labelledby="modal-role-menu-title" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-role-menu-title">Role Menu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="form-role-menu">
                        <div class="row overflow-auto" style="height:300px;">
                            <div class="col-md-12">
                                <input type="hidden" id="rm_position_id" name="rm_position_id">
                                <div class="accordion" id="accordion-menu">                                    
                                </div>
                            </div> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal menu -->
    
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Positions
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-success btn-sm ml-2" data-toggle="modal" id="btn-add" data-target="#modal-position">
                    <i class="fas fa-plus"></i>
                </button>
               
            </h6>
            @if (Auth::user()->store_id === 1)
            <div class="mb-3 ui-widget">
                <label for="unit" class="col-form-label">Store:</label>
                <select name="change_store" id="change_store" class="form-control reset">
                    @foreach ($stores as $store)
                        <option value="{{ $store->id }}">{{ $store->store }}</option>
                    @endforeach
                </select>
            </div>
        @endif
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="data-position" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Position Code</th>
                            <th>Position</th>
                            <th>Role Menu</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Position Code</th>
                            <th>Position</th>
                            <th>Role Menu</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection
<script src="{{ asset('vendor/jquery/jquery.min.js')}}"></script>
{{-- <script src="https://unpkg.com/html5-qrcode" type="text/javascript"></script> --}}
<script src="{{ asset('js/sweetalert.min.js')}}"></script>
<script>
    $(function () {
        let store_id = '{{ Auth::user()->store_id }}';

        const errorValidation = (err) => {
            let alert = '';
            const message = err;
            for (const key in message){
                    alert += `<div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <strong>Sory !</strong> ${message[key][0]}.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>`
                };
            $('#alert').html(alert);
        }

        $('#accordion-menu').sortable({
            axis: 'y',
            activate: function(event, ui){
                const id = $('.selector').data('id');
                //console.log(id)
            },
            update: function(event, ui){
                let data = [];
                $('.selector').each(function(i){
                    const id = $(this).data('id');
                    const key = $(this).data('number');
                    let obj = {
                        id:id,
                        key:key
                    }
                    data.push(obj);
                });

                $.ajax({
                    type: "POST",
                    url: "{{ url('menu/move/byrole') }}",
                    data: {sort: data},
                    headers: {
                        "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                        console.log(response)
                    }
                });
            }
        });

        const getData = (store_id) => {
            $('#data-position').DataTable({
                destroy: true,
                processing: true,
                serverSide: true,
                ajax: {
                    url: `{{ url('position/data') }}/${store_id}`,
                    type: "GET",
                    headers: {
                        "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
                    },
                    credentials: "same-origin"
                },
                columns: [
                    { data: "DT_RowIndex" },
                    { data: "position_code" },
                    { data: "position" },
                    { data: "id",
                      render: function(data, type, row, meta){
                        const btn = `<button id="btn-menu" data-position="${row.position}" data-id="${data}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="Add Menu">
                                        <i class="fas fa-stream"></i>
                                    </button>
                                    <button id="btn-sort" data-id="${data}" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Sort Menu">
                                        <i class="fas fa-layer-group"></i>
                                    </button>`;
                        return btn;
                      },
                      className: "text-center"
                    },
                    {
                        data: 'action', 
                        name: 'action', 
                        className: 'text-center',
                        orderable: true, 
                        searchable: true
                    }
                ]
            });
        }

        getData(store_id);

        $('#change_store').on('change', function(e){
            const change_store = $(this).val();
            getData(change_store);
        })

        const manage = (url, data, method) => {
            fetch(url, {
                method: method,
                headers: {
                    'Content-Type': 'application/json',
                    "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
                },
                credentials: "same-origin",
                body: JSON.stringify(data)
            })
            .then(res => res.json())
            .then((result) => {
                result.metadata.code === 200 && swal('Hey', result.metadata.message, 'success');
                result.metadata.code === 201 && errorValidation(result.metadata.message);
                $('#data-position').DataTable().ajax.reload();
                result.metadata.code === 200 && $('#modal-position').modal('hide');
            }).catch((err) => {
                console.log(err)
            });
        }

        const showAccordionMenu = (data, model) => {
            let childMenu = '';
            let accordionMenu = '';
            data.map((value, key) => {
                let menu_sts = value.status !== false ? 'checked':'';
                key++;
                if(value.child.length > 0){
                    let childList = '';
                    value.child.map((child, key_child) => {
                        let sts = child.status !== false?'checked':'';
                        let containChild = model === 'role'?
                        `<div class="custom-control custom-checkbox">
                            <input type="checkbox" ${sts} class="custom-control-input menu-checkbox" value="${child.id}" id="check-child-${child.menu}${child.id}">
                            <label class="custom-control-label" for="check-child-${child.menu}${child.id}">${child.menu}</label>
                        </div>`:
                        `<h6>${child.menu}</h6>`;
                        childList += `<li class="list-group-item selector-child" id="li-${key_child}" data-number=""${key_child}" data-id="${child.id}">
                                            ${containChild}
                                    </li>`;
                    });
                    childMenu += `
                                    <div id="${value.menu}${value.id}" class="collapse" aria-labelledby="${value.menu}" data-parent="#accordion-menu">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <ul class="list-group" id="list-${value.id}">
                                                        ${childList}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   `;
                } else {
                    childMenu = '';
                }
                let contain = model === 'role' ?
                `<div class="custom-control custom-checkbox">
                    <input type="checkbox" ${menu_sts} value="${value.id}" class="custom-control-input menu-checkbox" id="check-menu-${value.id}">
                    <label class="custom-control-label" for="check-menu-${value.id}">${value.menu}</label>
                </div>`:
                `<h6>${value.menu}</h6>`;
                accordionMenu += `<li id="li-${value.id}" data-id="${value.id}" data-number="${key}" class="selector">
                                    <div class="card" id="list-menu">
                                        <div class="card-header" id="heading${value.id}">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#${value.menu}${value.id}" aria-expanded="false" aria-controls="${value.menu}">
                                                ${contain}
                                            </button>
                                        </h2>
                                        </div>
                                        ${childMenu}
                                    </div>
                                  </li>`;
            });
            $('#accordion-menu').html(accordionMenu);
            $('.list-group').sortable({
                axis: 'y',
                activate: function(event, ui){
                    const id = $('.selector-child').data('id');
                    //console.log(id)
                },
                update: function(event, ui){
                    let data = [];
                    $('.selector-child').each(function(i){
                        const id = $(this).data('id');
                        const key = $(this).data('number');
                        let obj = {
                            id:id,
                            key:key
                        }
                        data.push(obj);
                    });

                    $.ajax({
                        type: "POST",
                        url: "{{ url('menu/move/byrole') }}",
                        data: {sort: data},
                        headers: {
                            "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response) {
                            console.log(response)
                        }
                    });
                }
            });
        }

        $('#btn-add').on('click', function(){
            $('#position_code').prop('readonly', false);
            $('.reset').val('');
        });

        $('#data-position').on('click', '#btn-menu', function(){
            const id = $(this).data('id');
            const position = $(this).data('position');
            $('#rm_position_id').val(id);
            $('#modal-role-menu-title').html(`Role Menu ${position}`);
            fetchGet(`{{ url('position/master/menu')}}/${store_id}/${id}`)
             .then(res => {
                res.data.length > 0 && showAccordionMenu(res.data, 'role');
                $('.menu-checkbox').change(function(e){
                    console.log(e)
                    const menu_id = $(this).val();
                    const data = {
                        position_id: id,
                        menu_id: menu_id,
                        store_id: store_id
                    }
                    if($(this).is(':checked')){
                        manageChoseRole('add', data);
                    } else {
                        manageChoseRole('delete', data);
                    }
                });
             })
             .catch(err => console.log(err));
            $('#modal-role-menu').modal('show');
        });

        $('#data-position').on('click', '#btn-sort', function(e){
            const id = $(this).data('id');
            $('#modal-role-menu-title').html('Sort Menu')
            fetchGet(`{{ url('menu/position/sort')}}?store_id=${store_id}&position_id=${id}`)
             .then(res => {
                res.data.length > 0 && showAccordionMenu(res.data, 'sort');
                $('#modal-role-menu').modal('show');
             })
             .catch(err => console.log(err));

        })

        const manageChoseRole = (sts, data) => {
            const url = sts === 'add' ? `{{ url('position/master/menu/addrole') }}`:
            `{{ url('position/master/menu/deleterole') }}`; 
            fetchPost(url, data)
            .then(res => {
                console.log(res);
            })
            .catch(err => console.log(err))
        }

        $('#data-position').on('click', '#btn-edit', function() {
            $('#position_code').prop('readonly', true);
            const id = $(this).data('id');
            const position = $(this).data('position');
            const position_code = $(this).data('position_code');
            $('#position_id').val(id);
            $('#position_code').val(position_code);
            $('#position').val(position);
            $('#modal-position').modal('show');
        })

        $('#data-position').on('click', '#btn-delete', function(){
            const id = $(this).data('id');
            swal({
                title: "Are you sure?",
                text: "Delete this data",
                icon: "warning",
                buttons: true,
                dangerMode: true,
                })
            .then((willDelete) => {
                if (willDelete) {
                    const url = `{{ url('position') }}/${id}`;
                    manage(url,{},'DELETE');
                    swal("Poof! Your data has been deleted!",'','success');
                } else {
                    swal("Cancel");
                }
            });
        })

        $('#btn-save').on('click', function(){
            const store = $('#store').val();
            const position_code = $('#position_code').val();
            const position = $('#position').val();
            const position_id = $('#position_id').val();
            let data = {
                store_id: store,
                position_code: position_code,
                position: position
            };
            let url = `{{route('position')}}`;
            if(position_id !== ''){
                data = {
                    position: position
                };
                url = `{{route('position')}}/${position_id}`
                manage(url, data, 'PUT');
                return;
            }
            
            manage(url, data, 'POST');
        });
        
    });
</script>