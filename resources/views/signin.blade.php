<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="png" href="{{ asset('img/logo-beyond-erp.png') }}">
    <title>Kondisi Usahaku </title>

    <!-- Custom fonts for this template-->
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lottie-web/5.8.1/lottie.min.js" integrity="sha512-V1YyTKZJrzJNhcKthpNAaohFXBnu5K9j7Qiz6gv1knFuf13TW/3vpgVVhJu9fvbdW8lb5J6czIhD4fWK2iHKXA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>    <!-- Custom styles for this template-->
    <link href="{{ asset('css/sb-admin-2.min.css')}}" rel="stylesheet">
<style>
    .shapes {
        width: 100px; height: 100px;position: absolute; top: 60%; right: 50%;
    }
</style>
</head>

<body class="bg-gradient-primary">

    <div class="container">
        
        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image" style="background: url({{asset('img/beyond-erp-login.svg')}});background-repeat: no-repeat;background-size: contain;background-position: center;"></div>
                            <div class="shapes" id="shapes"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                                    </div>
                                    @if(session()->has('success'))
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        {{ session('success')}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                    @endif
                                    @if(session()->has('loginError'))
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        {{ session('loginError')}}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                      </div>
                                    @endif
                                    <form class="user">
                                        @csrf
                                        <div class="form-group">
                                            <input type="text" class="form-control form-control-user @error('email') is-invalid @enderror"
                                                id="email" name="email" aria-describedby="email"
                                                placeholder="Enter Email..." autofocus required value="{{ old('email') }}">
                                                @error('email')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control form-control-user"
                                                id="password" name="password" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" id="customCheck">
                                                <label class="custom-control-label" for="customCheck">Remember
                                                    Me</label>
                                            </div>
                                        </div>
                                        <button type="button" id="btn-login" name="btn-login" class="btn btn-primary btn-user btn-block">
                                            <div id="label-login">Login</div>
                                            <div id="spinner" hidden style="width:1rem;height:1rem;" class="spinner-border text-warning" role="status">
                                                <span class="sr-only"></span>
                                            </div>
                                        </button>
                                        <hr>
                                        <a href="{{ route('oauth/redirect')}}" class="btn btn-google btn-user btn-block">
                                            <i class="fab fa-google fa-fw"></i> Login with Google
                                        </a>
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="forgot-password.html">Forgot Password?</a>
                                    </div>
                                    <div class="text-center">
                                        <a class="small" href="{{ url('register')}}">Create an Account!</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('js/sb-admin-2.min.js')}}"></script>
    <script src="{{ asset('vendor/sweetalert/dist/sweetalert.min.js') }}"></script>
    <script>
        var animations = bodymovin.loadAnimation({
            container: document.getElementById('shapes'),
            path: `{{ asset('json/bouncing-shapes.json')}}`,
            renderer: 'svg',
            loop: true,
            autoplay: true,
            name: "shapes"
        })
        $(document).ready(function () {
            const login = async (data) => {
                fetch(`login`, {
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/json',
                        "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
                    },
                    credentials: "same-origin",
                    body: JSON.stringify(data)
                })
                .then(res => res.json())
                .then((result) => {
                    $('#spinner').prop('hidden', true);
                    $('#label-login').prop('hidden', false);
                    result.metadata.code !== 200 && swal('sory', result.metadata.message, 'info');
                    if(result.metadata.code === 200){
                        localStorage.setItem("token", result.token);
                        window.location = `{{ url('/')}}`
                    }
                }).catch((err) => {
                    console.log(err.name)
                    console.log(err.message)
                    swal('Sory',err.message, 'error');
                });
            }
            $('#btn-login').click(() => {
                const email = document.getElementById('email').value;
                const password = document.getElementById('password').value;
                const data = {
                    email: email,
                    password: password,
                }
                $('#spinner').prop('hidden', false);
                $('#label-login').prop('hidden', true);
                login(data);
            });

            //$('#customCheck').prop('checked', true);
        });
    </script>
</body>

</html>