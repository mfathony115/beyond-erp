<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="png" href="{{ asset('img/logo-beyond-erp.png') }}">

    <title>Kondisi Usahaku</title>

    <!-- Custom fonts for this template-->
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row">
                    <div class="col-lg-5 d-none d-lg-block bg-register-image" style="background: url({{asset('img/beyond-erp-register.svg')}});background-size: inherit;background-position:center;background-repeat:no-repeat;"></div>
                    <div class="col-lg-7">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                            </div>
                            <div class="col-md-12" id="alert"></div>
                            <form id="form-register" class="user">
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" name="username" id="username" placeholder="Username">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control form-control-user" name="fullname" id="fullname" placeholder="Fullname">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control form-control-user" name="email" id="email" placeholder="Email Address">
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="npwp" class="form-control form-control-user" name="npwp" id="npwp" placeholder="Please enter NPWP if you have ...">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="password" class="form-control form-control-user" name="password" id="password" placeholder="Password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" name="store_name" id="store_name"
                                            placeholder="Store name">
                                    </div>
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" name="phone" id="phone"
                                            placeholder="Phone number">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12 mb-3 mb-sm-0">
                                       <textarea placeholder="Address" class="form-control form-control-user" name="address" id="address"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div id="recaptcha-container"></div>
                                </div>
                                <button type="button" id="btn-register" class="btn btn-primary btn-user btn-block">
                                    <div id="label-register">Register Account</div>
                                    <div id="spinner" hidden style="width:1rem;height:1rem;" class="spinner-border text-warning" role="status">
                                        <span class="sr-only"></span>
                                    </div>
                                </button>
                            </form>
                            <hr>
                            <div class="text-center">
                                <a class="small" href="forgot-password.html">Forgot Password?</a>
                            </div>
                            <div class="text-center">
                                <a class="small" href="{{ url('login')}}">Already have an account? Login!</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('vendor/sweetalert/dist/sweetalert.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
    <script defer src="https://www.gstatic.com/firebasejs/8.10.1/firebase-app.js"></script>
    <script defer src="https://www.gstatic.com/firebasejs/8.10.1/firebase-analytics.js"></script>
    <script defer src="https://www.gstatic.com/firebasejs/8.10.1/firebase-auth.js"></script>
    <script defer src="https://www.gstatic.com/firebasejs/8.10.1/firebase-firestore.js"></script>
    <script type="module">
        // window.onload = function () {
        //     render();
        // };
        // const firebaseConfig = {
        //   apiKey: "AIzaSyDOOomGQGkPlbiEPMG0GDWITZvu6dm6Us0",
        //   authDomain: "kondisi-usahaku.firebaseapp.com",
        //   projectId: "kondisi-usahaku",
        //   storageBucket: "kondisi-usahaku.appspot.com",
        //   messagingSenderId: "855716361201",
        //   appId: "1:855716361201:web:1c915b6ed32d744a76ca50",
        //   measurementId: "G-VH8XT27J86"
        // };
      
        // Initialize Firebase
        // const app = firebase.initializeApp(firebaseConfig);
        // firebase.analytics();
        // const phoneNumber = document.querySelector("#phone");
       
        // const buttonRegister = document.querySelector("#btn-registers");
    
        // const render = () => {
        //     window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
        //         'size': 'invisible'
        //     });
        //     recaptchaVerifier.render().then((widgetId) => {
        //         window.recaptchaWidgetId = widgetId;
        //     });
        // }

        // buttonRegister.addEventListener("click", () => {
        //     let number = phoneNumber.value;
        //     firebase.auth().signInWithPhoneNumber(number, window.recaptchaVerifier)
        //     .then((confirmationResult) => {
            // SMS sent. Prompt user to type the code from the message, then sign the
            // user in with confirmationResult.confirm(code).
            // window.confirmationResult = confirmationResult;
            // console.log(confirmationResult)
            // ...
            // }).catch((error) => {
            //     console.log(error)
            // Error; SMS not sent
            // ...
        //     });
        // });
    </script>
    <script>
        $(document).ready(function () {
            const errorValidation = (err) => {
                let alert = '';
                const message = err;
                for (const key in message){
                    alert += `<div class="alert alert-warning alert-dismissible fade show" role="alert">
                                <strong>Sory !</strong> ${message[key][0]}.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>`
                };
                $('#alert').html(alert);
            }

            const delay = ms => new Promise(res => setTimeout(res, ms));

            const submitForm = () => {
                var formregister = document.forms.namedItem("form-register")
                $.ajax({
                   type: "POST",
                   processData:false,
                   contentType:false,
                   cache:false,
                   async:true,
                   crossOrigin : true,
                   headers: {
                    "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content')
                   },
                   url: "{{url('register')}}",
                   data: new FormData(formregister),
                   dataType: "JSON",
                   beforeSend: function(e){
                        $('#label-register').prop('hidden', true);
                        $('#spinner').prop('hidden', false);
                        $('#btn-register').prop('disabled', true);
                   },
                   success: async function(response) {
                        $('#btn-register').prop('disabled', false);
                        $('#label-register').prop('hidden', false);
                        $('#spinner').prop('hidden', true);
                        response.metadata.code === 201 && errorValidation(response.metadata.message);
                        if(response.metadata.code === 200){
                            swal('success','please check email to verify your account', 'success');
                            await delay(2000);
                            window.location = `{{url('login')}}`
                        }
                   },
                   error: function(err){
                    console.log(err)
                   }
               });
            }

            $('#btn-register').on('click', function(){
                submitForm();
            })
        });
    </script>
</body>

</html>