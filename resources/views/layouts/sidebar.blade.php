<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon">
            <img src="{{asset('img/ku-logo.png')}}" width="50" height="50" alt="ku-logo.png" class="">
        </div>
        <div class="sidebar-brand-text">Help Everyone</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">
    <div class="sidebar-role" id="sidebar-role">
    </div>
        <!-- Nav Item - Dashboard -->
        {{-- <li class="nav-item {{ Request::segment(1) === 'dashboard'? 'active': ''}}">
            <a class="nav-link" href="{{ route('dashboard')}}">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span id="title-dashboard">Dashboard</span></a>
        </li> --}}

        <!-- Divider -->
        {{-- <hr class="sidebar-divider">
        <div class="sidebar-heading">
            Settings
        </div> --}}
        {{-- <li class="nav-item {{ Request::segment(1) === 'setting'? 'active': ''}}">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSetting"
                aria-expanded="true" aria-controls="collapseSetting">
                <i class="fas fa-fw fa-cog"></i>
                <span>Setting</span>
            </a>
            <div id="collapseSetting" class="collapse {{ Request::segment(1) === 'setting'? 'show': ''}}" aria-labelledby="Setting" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Setting Menu :</h6>
                    <a class="collapse-item {{ Request::segment(2) === 'user' ? 'active' : ''}}" href="{{ route('setting/user') }}">Users</a>
                    <a class="collapse-item {{ Request::segment(2) === 'product' ? 'active' : ''}}" href="{{ route('setting/product') }}">Product</a>
                    <a class="collapse-item {{ Request::segment(2) === 'position' ? 'active' : ''}}" href="{{ route('setting/position') }}">Positions</a>
                    <a class="collapse-item {{ Request::segment(2) === 'category' ? 'active' : ''}}" href="{{ route('setting/category') }}">Category</a>
                    <a class="collapse-item {{ Request::segment(2) === 'merk' ? 'active' : ''}}" href="{{ route('setting/merk') }}">Merk</a>
                    <a class="collapse-item {{ Request::segment(2) === 'unit' ? 'active' : ''}}" href="{{ route('setting/unit') }}">Unit</a>
                    <a class="collapse-item {{ Request::segment(2) === 'menu' ? 'active' : ''}}" href="{{ route('setting/menu') }}">Menu</a>
                    <a class="collapse-item {{ Request::segment(2) === 'warehouse' ? 'active' : ''}}" href="{{ route('setting/warehouse') }}">Warehouse</a>

                </div>
            </div>
        </li> --}}
    {{-- </div> --}}

    <!-- Divider -->
    {{-- <hr class="sidebar-divider d-none d-md-block"> --}}

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

    <!-- Sidebar Message -->
    <div class="sidebar-card d-none d-lg-flex">
        <img class="sidebar-card-illustration mb-2" src="{{ asset('img/undraw_rocket.svg') }}" alt="...">
        <p class="text-center mb-2"><strong>SB Admin Pro</strong> is packed with premium features, components, and more!</p>
        <a class="btn btn-success btn-sm" href="https://startbootstrap.com/theme/sb-admin-pro">Upgrade to Pro!</a>
    </div>

</ul>