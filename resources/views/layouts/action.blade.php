<button type="button" data-toggle="tooltip" data-placement="top" title="Barcode" class="btn btn-primary btn-circle btn-sm" id="btn-barcode" data-id="{{$id}}" data-product_code="{{ $model->product_code }}">
    <i class="fas fa-barcode"></i>
</button>
<button type="button" data-toggle="tooltip" data-placement="top" title="Edit" id="btn-edit" data-id="{{ $id }}" class="btn btn-circle btn-success btn-sm">
    <i class="far fa-edit"></i>
</button> 
<button id="btn-delete" data-toggle="tooltip" data-placement="top" title="Delete" data-id="{{ $id }}" class="btn btn-danger btn-circle btn-sm">
    <i class="fas fa-trash"></i>
</button>