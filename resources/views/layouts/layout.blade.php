<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="png" href="{{ asset('img/logo-beyond-erp.png') }}">
    <title>Kondisi Usahaku</title>

    <!-- Custom fonts for this template-->
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{ asset('css/sb-admin-2.min.css')}}" rel="stylesheet">
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/jquery-ui/css/jquery-ui.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('vendor/datetimepicker/css/datetimepicker.min.css') }}" />

<style>
.preloader {
  width: 100%;
  height: 100vh;
  background-color: #fff;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 99999;
  position: fixed;
}
.preloader .dot {
  width: 10px;
  height: 10px;
  border-radius: 100%;
  background-color: #004489;
  position: absolute;
  left: 30%;
  -webkit-animation: easeInAndOut 2.5s ease infinite;
          animation: easeInAndOut 2.5s ease infinite;
}
.preloader .dot:nth-child(1) {
  -webkit-animation-delay: 0s;
          animation-delay: 0s;
  opacity: 0;
}
.preloader .dot:nth-child(2) {
  -webkit-animation-delay: 0.3s;
          animation-delay: 0.3s;
  opacity: 0.8;
}
.preloader .dot:nth-child(3) {
  -webkit-animation-delay: 0.6s;
          animation-delay: 0.6s;
  opacity: 0.5;
}
.preloader .dot:nth-child(4) {
  -webkit-animation-delay: 0.9s;
          animation-delay: 0.9s;
  opacity: 0.2;
}

@-webkit-keyframes easeInAndOut {
  0% {
    left: 30%;
    opacity: 0;
  }
  48% {
    left: 49%;
    opacity: 1;
  }
  50% {
    left: 51%;
    opacity: 1;
  }
  100% {
    left: 70%;
    opacity: 0;
  }
}

@keyframes easeInAndOut {
  0% {
    left: 30%;
    opacity: 0;
  }
  48% {
    left: 49%;
    opacity: 1;
  }
  50% {
    left: 51%;
    opacity: 1;
  }
  100% {
    left: 70%;
    opacity: 0;
  }
}
</style>
</head>
<body id="page-top">
    <div class="preloader">
        <div class="dot"></div>
        <div class="dot"></div>
        <div class="dot"></div>
        <div class="dot"></div>
    </div>
    <!-- Page Wrapper -->
    <div id="wrapper">
        
        <!-- Sidebar -->
        @include('layouts.sidebar')
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                @include('layouts.topbar')
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                @yield('content')
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            @include('layouts.footer')
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up" style="line-height:unset;"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <form action="{{route('logout')}}" method="post">
                        @csrf
                        <button class="btn btn-primary" type="submit">Logout</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{ asset('js/sb-admin-2.min.js')}}"></script>

    <!-- Page level plugins -->
    <script src="{{ asset('vendor/chart.js/Chart.min.js')}}"></script>
    <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Bootstrap Select -->
    <script src="{{ asset('vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('vendor/numeral/js/numeral.min.js') }}"></script>
    <script src="{{ asset('js/fetch-config.js')}}"></script>
    <script src="{{ asset('vendor/jquery-ui/js/jquery-ui.min.js')}}"></script>
    
    <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('vendor/datetimepicker/js/datetimepicker.min.js') }}"></script>

    <script>
        window.addEventListener('DOMContentLoaded', (event) => {
            console.log('DOM fully loaded and parsed');
        });
        $(window).on('load', function () {
            if ($(".preloader").length > 0){
                $(".preloader").fadeOut("slow");
            }
            const store_id = `{{ Auth::user()->store_id }}`;
            const position_id = `{{ Auth::user()->position_id }}`;
            const data = {
                store_id: store_id,
                position_id: position_id
            }
            fetchPost(`{{ url('menu/position/role') }}`, data)
            .then(res => {
                if(res.metadata.code === 200){
                    showMenu(res.data);
                }
            })
            .catch(err => console.log(err));

            const showMenu = data => {
              let menu = '';
              let devider = '';
              let childMenu = '';
                data.map((value, key) => {
                  if(value.child.length > 0){
                    value.child.map((child, key) => {
                        const urlmenu = child.url === '#' ? '#': `{{ url('${child.url}')}}`;
                        childMenu += `<a class="collapse-item" href="${urlmenu}">${child.menu}</a>`
                    })
                    
                    menu += `${devider}
                            <li class="nav-item">
                                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse${value.menu}"
                                    aria-expanded="true" aria-controls="collapseSetting">
                                    <i class="fas fa-fw fa-cog"></i>
                                    <span>Setting</span>
                                </a>
                                <div id="collapse${value.menu}" class="collapse" aria-labelledby="Setting" data-parent="#accordionSidebar">
                                    <div class="bg-white py-2 collapse-inner rounded">
                                        <h6 class="collapse-header">${value.menu} :</h6>
                                        ${childMenu}
                                    </div>
                                </div>
                            </li>
                            <hr class="sidebar-divider my-0">`
                  } else {
                    menu += `
                            
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    <i class="fas fa-fw fa-tachometer-alt"></i>
                                    <span id="title-dashboard">${value.menu}</span></a>
                            </li>
                            <hr class="sidebar-divider my-0">`;
                  }
                });
              $('#sidebar-role').html(menu);
            }
        });
    </script>
</body>

</html>