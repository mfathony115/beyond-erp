<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
class Message extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = User::where('email', $this->request->email)->first();
        $data = [
            'id' => $user->id,
            'time' => strtotime($user->updated_at)
        ];
        return $this->from("backupmfathony@gmail.com", 'verification')
            ->markdown('email')
            ->with([
                'data' => $data,
                'username' => $user->username
            ]);
    }
}
