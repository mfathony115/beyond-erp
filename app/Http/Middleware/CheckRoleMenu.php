<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Repositories\Master\RoleMenuRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class CheckRoleMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    
    protected $roleMenu;
    public function __construct(RoleMenuRepository $roleMenu)
    {
        $this->roleMenu = $roleMenu;
    }
     
    public function handle(Request $request, Closure $next)
    {
        $home = url('/');
        $first = URL::current();
        $e = str_replace($home, '',$first);
        $real_url = substr($e,1);
        $position_id = Auth::user()->position_id;
        $store_id = Auth::user()->store_id;
        $data = $this->roleMenu->getRoleMenuByPosition_($store_id, $position_id);
        $arr_col = array_column($data->toArray(), 'url');
        $search = array_search($real_url, $arr_col);
        if($search === false){
            return abort(404);
        }

        return $next($request);
    }
}
