<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\LoginService;
class LoginController extends Controller
{
    protected $loginService;
    
    public function __construct(LoginService $loginService)
    {
        $this->loginService = $loginService;
    }
    
    public function index()
    {
        return view('signin');
    }

    public function authenticate(Request $request)
    {
        $data = $this->loginService->login($request);
        return $data;
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }

    public function oAuthGoogle()
    {
        $data = $this->loginService->oAuthGoogle();
        return $data;
    }

    public function redirectOauth()
    {
        $data = $this->loginService->redirectToProvider();
        return $data;
    }
}
