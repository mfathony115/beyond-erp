<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\Message;
use App\Models\Master\Stores;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Services\RegisterService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Socialite;

class RegisterController extends Controller
{
    protected $registerService;
    
    public function __construct(RegisterService $registerService)
    {
        $this->registerService = $registerService;
    }
    
    public function index(Request $request)
    {
        return view('signup');
    }

    public function register(Request $request)
    {
        $data = $this->registerService->register($request);
        return $data;
    }

    public function messageEmail()
    {
        $user = Auth::user();
        $data['data'] = [
            'id' => $user->id,
            'time' => strtotime($user->updated_at)
        ];
        $data['username'] = $user->username;
        return view('email', $data);
    }
    
    public function verified(Request $request)
    {
        // $check = Auth::user()->email_verified_at;
        // if($check === null){
        //     return redirect('login')->with('status', 'silakan aktivasi akun di email');
        // }
        $data['data'] = [
            'id' => Auth::user()->id,
            'time' => strtotime(Auth::user()->updated_at)
        ];
        return view('verificationNotice', $data);
        //Mail::to('mfathony115@gmail.com')->send(new VerificationNotice());
    }

    public function verifying(Request $request)
    {
        $key = $request->get('key');
        if(!isset($key)){
            $res = [
                'metadata' => [
                    'code' => 402,
                    'message' => 'key is required'
                ]
            ];
            return response($res,402);
        }
        $dec_start = base64_decode($key);
        $decode = json_decode($dec_start);
        $time = date('Y-m-d H:i:d', $decode->time);
        $start = Carbon::parse($time);
        $verif = $start->addMinutes(30);
        if(time() > strtotime($verif)){
            $data['data'] = [
                'activation_created' => $verif->format('d-m-Y H:i:s'),
                'now' => Carbon::now()->format('d-m-Y H:i:s')
            ];
            return view('message', $data);
        } else {
            $update = User::findOrFail($decode->id);
            $getStore = Stores::where('email', $update->email)->first();
            $getStore->status_data = 0;
            $getStore->save();
            $update->status_data = 0;
            $update->store_id = $getStore->id;
            $update->email_verified_at = Carbon::now();
            $update->created_at = Carbon::now();
            $update->save();
            $this->registerService->addDefaultMenu($getStore->id, $update->username, $update->position_id);
            return redirect('/');
        }
    }

    public function resendingEmail(Request $request)
    {
        $auth = Auth::user();
        $user = User::findOrFail($auth->id);
        $user->updated_at = Carbon::now();
        $user->save();
        $data['data'] = [
            'id' => $auth->id,
            'time' => strtotime($auth->created_at)
        ];
        $data['username'] = $auth->username;
        try {
            Mail::to($user->email)->send(new Message($user));
            return redirect('/');
        } catch (\Exception $e) {
            return response($e->getMessage());
        }
    }

    public function redirectToGoogle(){
        return Socialite::driver('google')
            ->stateless()
            ->redirect();
    }
}
