<?php
# done
namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\Positions;
use Illuminate\Http\Request;
use App\Services\Master\UserService;
use App\Services\Master\StoreService;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $userService;
    protected $storeService;
    public function __construct(UserService $userService)
    {
        $this->storeService = new StoreService();
        $this->userService = $userService;
    }

    public function index()
    {
        $store_id = Auth::user()->store_id;
        $data['positions'] = $store_id !== 1 ? Positions::where('store_id', $store_id)->get():Positions::all();
        $data['stores'] = $store_id !== 1 ? 
        $this->storeService->getById($store_id):
        $this->storeService->getAll();
        return view('master.user', $data);
    }
    
    public function getAll()
    {
        $data = $this->userService->getAll();
        return $data;
    }

    public function allData(Request $request, $id)
    {
        $data = $this->userService->allData($request, $id);
        return $data;
    }

    public function getById(Request $request, $id)
    {
        $data = $this->userService->getById($request, $id);
        return $data;
    }

    public function store(Request $request)
    {
        $data = $this->userService->store($request);
        return $data;
    }

    public function update(Request $request, $id)
    {
        $data = $this->userService->update($request, $id);
        return $data;
    }

    public function delete($id)
    {
        $delete = $this->userService->delete($id);
        return $delete;
    }
}
