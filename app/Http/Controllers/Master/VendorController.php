<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\Stores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\Master\VendorService;

class VendorController extends Controller
{
    protected $vendorService;
    
    public function __construct(VendorService $vendorService)
    {
        $this->vendorService = $vendorService;
    }
    

    public function index(Request $request)
    {
        $store_id = Auth::user()->store_id;
        $data['stores'] = $store_id !== 1 ? 
        Stores::where('id', $store_id)->firstOrFail():
        Stores::all();
        return view('master.vendor', $data);
    }

    public function allData(Request $request, $id)
    {
        $data = $this->vendorService->allData($request,$id);
        return $data;
    }

    public function searchData(Request $request)
    {
        $data = $this->vendorService->searchData($request);
        return $data;
    }

    public function data()
    {
        $data = $this->vendorService->data();
        return $data;
    }

    public function store(Request $request)
    {
        $data = $this->vendorService->store($request);
        return $data;
    }

    public function update(Request $request, $id)
    {
        $update = $this->vendorService->update($request, $id);
        return $update;
    }

    public function delete($id)
    {
        $delete = $this->vendorService->delete($id);
        return $delete;
    }
}
