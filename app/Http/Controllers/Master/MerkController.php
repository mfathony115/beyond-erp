<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\Stores;
use Illuminate\Http\Request;
use App\Services\Master\MerkService;
use Illuminate\Support\Facades\Auth;

class MerkController extends Controller
{

    protected $merkService;
    
    public function __construct(MerkService $merkService)
    {
        $this->merkService = $merkService;
    }
    
    public function index(Request $request)
    {
        $store_id = Auth::user()->store_id;
        $data['stores'] = $store_id !== 1 ? 
        Stores::where('id', $store_id)->firstOrFail():
        Stores::all();
        return view('master.merk', $data);
    }

    public function allData(Request $request, $id)
    {
        $data = $this->merkService->allData($request,$id);
        return $data;
    }

    public function data()
    {
        $data = $this->merkService->data();
        return $data;
    }

    public function store(Request $request)
    {
        $data = $this->merkService->store($request);
        return $data;
    }

    public function update(Request $request, $id)
    {
        $update = $this->merkService->update($request, $id);
        return $update;
    }

    public function delete($id)
    {
        $delete = $this->merkService->delete($id);
        return $delete;
    }
}
