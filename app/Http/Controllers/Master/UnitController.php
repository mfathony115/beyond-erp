<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\Stores;
use Illuminate\Http\Request;
use App\Services\Master\UnitService;
use Illuminate\Support\Facades\Auth;

class UnitController extends Controller
{
    protected $unitService;
    
    public function __construct(UnitService $unitService)
    {
        $this->unitService = $unitService;
    }
    
    public function index(Request $request)
    {
        $store_id = Auth::user()->store_id;
        $data['stores'] = $store_id !== 1 ? 
        Stores::where('id', $store_id)->firstOrFail():
        Stores::all();
        return view('master.unit', $data);
    }

    public function allData(Request $request, $id)
    {
        $data = $this->unitService->allData($request,$id);
        return $data;
    }

    public function store(Request $request)
    {
        $data = $this->unitService->store($request);
        return $data;
    }

    public function update(Request $request, $id)
    {
        $update = $this->unitService->update($request, $id);
        return $update;
    }

    public function delete($id)
    {
        $delete = $this->unitService->delete($id);
        return $delete;
    }
}
