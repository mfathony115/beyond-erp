<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\Stores;
use Illuminate\Http\Request;
use App\Services\Master\PositionService;
use Illuminate\Support\Facades\Auth;

class PositionsController extends Controller
{
    protected $positionService;
    
    public function __construct(PositionService $positionService)
    {
        $this->positionService = $positionService;
        // $this->middleware('checkRole');
    }
    
    public function index()
    {
        $store_id = Auth::user()->store_id;
        $data['stores'] = $store_id !== 1 ? 
        Stores::where('id', $store_id)->get():
        Stores::all();
        return view('master.positions', $data);
    }

    public function store(Request $request)
    {
        $insert = $this->positionService->store($request);
        return $insert;
    }

    public function allData(Request $request, $id)
    {
        $data = $this->positionService->allData($request,$id);
        return $data;
    }

    // public function getAll(Request $request, $id)
    // {
    //     $data = $this->positionService->getAll($request, $id);
    //     return $data;
    // }

    public function update(Request $request, $id)
    {
        $update = $this->positionService->update($request, $id);
        return $update;
    }

    public function delete($id)
    {
        $delete = $this->positionService->delete($id);
        return $delete;
    }

    public function getMasterMenu($id,$position_id)
    {
        $data = $this->positionService->getMasterMenu($id, $position_id);
        return $data;
    }

    public function addRoleByPosition(Request $request)
    {
        $data = $this->positionService->addRoleByPosition($request);
        return $data;
    }

    public function deleteRoleByPosition(Request $request)
    {
        $data = $this->positionService->deleteRoleByPosition($request);
        return $data;
    }

    public function roleByPosition(Request $request)
    {
        $data = $this->positionService->roleByPosition($request);
        return $data;
    }

    public function sortRoleMenu(Request $request)
    {
        $data = $this->positionService->getRoleMenuByPosition_($request);
        return $data;
    }

    public function moveMenuByRole(Request $request)
    {
        $data = $this->positionService->moveMenuByRole($request);
        return $data;
    }
}
