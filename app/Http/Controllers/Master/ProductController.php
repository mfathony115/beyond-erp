<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\Category;
use App\Models\Master\Merk;
use App\Models\Master\Stores;
use App\Models\Master\Unit;
use Illuminate\Http\Request;
use App\Services\Master\ProductService;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    protected $productService;
    
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function index(Request $request)
    {
        $store_id = Auth::user()->store_id;
        $data['stores'] = $store_id !== 1 ? Stores::where('id', $store_id)->get():Stores::all();
        $data['category'] = $store_id !== 1 ? Category::where('store_id', $store_id)->get():Category::all();
        $data['unit'] = $store_id !== 1 ? Unit::where('store_id', $store_id)->get():Unit::all();
        $data['merk'] = $store_id !== 1 ? Merk::where('store_id', $store_id)->get():Merk::all();
        return view('master.product', $data);
    }

    public function testBase64()
    {
        $data = file_get_contents('result_base64.txt');
        $dec = base64_decode($data);
        header('Content-Type: application/pdf');
        echo $dec;
    }

    public function allData(Request $request, $id)
    {
        $data = $this->productService->allData($request,$id);
        return $data;
    }

    public function getById($id)
    {
        $data = $this->productService->getById($id);
        return $data;
    }

    public function store(Request $request)
    {
        $data = $this->productService->store($request);
        return $data;
    }

    public function update(Request $request, $id)
    {
        $update = $this->productService->update($request, $id);
        return $update;
    }

    public function delete($id)
    {
        $delete = $this->productService->delete($id);
        return $delete;
    }
    
}
