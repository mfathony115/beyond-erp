<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Master\MenuService;

class MenuController extends Controller
{
    protected $menuService;
    
    public function __construct(MenuService $menuService)
    {
        $this->menuService = $menuService;
    }
    

    public function index(Request $request)
    {
        return view('master.menu');
    }

    public function allData(Request $request, $id)
    {
        $data = $this->menuService->allData($request,$id);
        return $data;
    }

    public function getAll()
    {
        $data = $this->menuService->getAll();
        return $data;
    }

    public function store(Request $request)
    {
        $data = $this->menuService->store($request);
        return $data;
    }

    public function update(Request $request, $id)
    {
        $update = $this->menuService->update($request, $id);
        return $update;
    }

    public function delete($id)
    {
        $delete = $this->menuService->delete($id);
        return $delete;
    }
}
