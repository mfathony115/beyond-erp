<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\Models\Master\Stores;
use Illuminate\Http\Request;
use App\Services\Master\CategoryService;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    protected $categoryService;
    
    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index(Request $request)
    {
        $store_id = Auth::user()->store_id;
        $data['stores'] = $store_id !== 1 ? 
        Stores::where('id', $store_id)->firstOrFail():
        Stores::all();
        return view('master.category', $data);
    }

    public function allData(Request $request, $id)
    {
        $data = $this->categoryService->allData($request,$id);
        return $data;
    }

    public function getAll(Request $request, $id)
    {
        $data = $this->categoryService->getAll($id);
        return $data;
    }

    public function store(Request $request)
    {
        $data = $this->categoryService->store($request);
        return $data;
    }

    public function update(Request $request, $id)
    {
        $update = $this->categoryService->update($request, $id);
        return $update;
    }

    public function delete($id)
    {
        $delete = $this->categoryService->delete($id);
        return $delete;
    }
    
}
