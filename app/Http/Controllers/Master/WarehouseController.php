<?php

namespace App\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Master\WarehouseService;

class WarehouseController extends Controller
{
    protected $warehouseService;
    
    public function __construct(WarehouseService $warehouseService)
    {
       $this->warehouseService = $warehouseService;
    }

    public function index(Request $request)
    {
        return view('master.warehouse');
    }

    public function allData(Request $request, $id)
    {
        $data = $this->warehouseService->allData($request,$id);
        return $data;
    }

    // public function getAll(Request $request, $id)
    // {
    //     $data = $this->warehouseService->getAll($id);
    //     return $data;
    // }

    public function store(Request $request)
    {
        $data = $this->warehouseService->store($request);
        return $data;
    }

    public function update(Request $request, $id)
    {
        $update = $this->warehouseService->update($request, $id);
        return $update;
    }

    public function delete($id)
    {
        $delete = $this->warehouseService->delete($id);
        return $delete;
    }
    
}
