<?php

namespace App\Services\Master;
use App\Repositories\Master\StoreRepository;

class StoreService{

    protected $storeRepository;
    
    public function __construct()
    {
        $this->storeRepository = new StoreRepository();
    }
    

    public function getAll()
    {
        $data = $this->storeRepository->getAll();
        return $data;
    }

    public function getById($id)
    {
        $data = $this->storeRepository->getById($id);
        return $data;
    }
}