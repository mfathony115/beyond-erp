<?php

namespace App\Services\Master;

use App\Repositories\Master\UnitRepository;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UnitService
{
    protected $unitRepository;
    
    public function __construct(UnitRepository $unitRepository)
    {
        $this->unitRepository = $unitRepository;
    }
    
    public function allData($request, $status)
    {
        $data = $status === 'all' ? $this->unitRepository->getAll():
        $this->unitRepository->getByStore($request);

        if($request->ajax()){
            $data = $this->unitRepository->getAll($request, $status);
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<button type="button" id="btn-edit" 
                    data-id="'.$row->id.'"
                    data-unit_code="'.$row->unit_code.'"
                    data-unit="'.$row->unit.'"
                    class="btn btn-circle btn-success btn-sm"><i class="far fa-edit"></i></button> 
                    <button id="btn-delete" 
                    data-id="'.$row->id.'"
                    class="btn btn-danger btn-circle btn-sm"><i class="fas fa-trash"></i></button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }
    
    public function store($request)
    {
        $rules = [
            'unit_code' => 'required',
            'unit' => 'required',
            'store_id' => 'required'
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        $obj = [
            'store_id' => $request->store_id,
            'unit_code' => $request->unit_code,
            'unit' => $request->unit,
            'user_created' => Auth::user()->username,
            'user_updated' => Auth::user()->username
        ];
        try {
            $data = $this->unitRepository->store($obj);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success insert'
                ],
                'data' => $data
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'insert failed',
                    'log' => $e->getMessage()
                ]
            ];
        }

        return response($res);
    }

    public function update($request, $id)
    {
        $rules = [
            'unit' => 'required'
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        try {
            $update = $this->unitRepository->update($request, $id);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success update'
                ],
                'data' => $update
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'failed update',
                    'log' => $e->getMessage()
                ]
            ];
        }
        return $res;
    }

    public function delete($id)
    {
        try {
            $delete = $this->unitRepository->delete($id);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success delete'
                ],
                'data' => $delete
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'failed delete',
                    'log' => $e->getMessage()
                ]
            ];
        }
        return $res;
    }
}