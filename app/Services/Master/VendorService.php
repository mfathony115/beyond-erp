<?php

namespace App\Services\Master;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Repositories\Master\VendorRepository;
use DataTables;

class VendorService
{
    protected $vendorRepository;
    
    public function __construct(VendorRepository $vendorRepository)
    {
        $this->vendorRepository = $vendorRepository;
    }
    

    public function allData($request, $status)
    {
        $data = $status === 'all' ? $this->vendorRepository->getAll():
        $this->vendorRepository->getByStore($request);
        
       // if($request->ajax()){
            return DataTables::of($data)
                ->addIndexColumn()
                ->make(true);
        //}
    }

    public function data()
    {
        $data = $this->vendorRepository->getAll();
        return response($data);
    }

    public function searchData($request)
    {
        $rules = [
            'name' => 'required'
        ];
        $validation = Validator::make($request->all(), $rules);
        if($validation->fails()){
            $res = [
                'metadata' => [
                    'code' => 201,
                    'message' => $validation->errors()
                ]
            ];
            return response($res, $res['metadata']['code']);
        }
        $name = $request->get('name');
        $data = $this->vendorRepository->searchData($name);
        $res = [
            'metadata' => [
                'code' => 209,
                'message' => 'data not found'
            ]
        ];
        if(count($data) > 0){
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success'
                ],
                'data' => $data
            ];
        }
        return response($res);
    }
    
    public function store($request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required',
            'telp' => 'required',
            'address' => 'required',
            'tipe' => 'required',
            'store_id' => 'required'
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        $obj = [
            'store_id' => $request->store_id,
            'name' => $request->name,
            'email' => $request->email,
            'telp' => $request->telp,
            'address' => $request->address,
            'npwp' => $request->npwp??'-',
            'tipe' => $request->tipe === 'perusahaan' ? 0 : 1,
            'user_created' => Auth::user()->username?? auth('api')->user()->username,
            'user_updated' => Auth::user()->username?? auth('api')->user()->username
        ];
        try {
            $data = $this->vendorRepository->store($obj);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success insert'
                ],
                'data' => $data
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'insert failed',
                    'log' => $e->getMessage()
                ]
            ];
        }

        return response($res, $res['metadata']['code']);
    }

    public function update($request, $id)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required',
            'telp' => 'required',
            'address' => 'required',
            'tipe' => 'required',
            'store_id' => 'required'
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        try {
            $update = $this->vendorRepository->update($request, $id);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success update'
                ],
                'data' => $update
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'failed update',
                    'log' => $e->getMessage()
                ]
            ];
        }
        return response($res, $res['metadata']['code']);
    }

    public function delete($id)
    {
        try {
            $delete = $this->vendorRepository->delete($id);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success delete'
                ],
                'data' => $delete
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'failed delete',
                    'log' => $e->getMessage()
                ]
            ];
        }
        return response($res, $res['metadata']['code']);
    }
}