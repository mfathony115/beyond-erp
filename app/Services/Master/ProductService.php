<?php

namespace App\Services\Master;
use App\Repositories\Master\ProductRepository;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProductService
{
    protected $productRepository;
    
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }
    

    public function allData($request, $status)
    {
        $data = $status === 1 ? $this->productRepository->getAll():
        $this->productRepository->getByStore($status);
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', 'layouts.action')
            ->rawColumns(['action'])
            ->make(true);
        
    }

    public function getById($id)
    {
        $data = $this->productRepository->getById($id);
        $response = [
            'metadata' => [
                'code' => 200,
                'message' => 'success'
            ],
            'data' => $data,
        ];
        return response($response);
    }
    
    public function store($request)
    {
        $rules = [
            'product_code' => 'required:unique:product',
            'product_name' => 'required',
            'unit_id' => 'required',
            'unit' => 'required',
            'category_id' => 'required',
            'category' => 'required',
            'merk_id' => 'required',
            'merk' => 'required',
            'bkp' => 'required',
            'purchase_price' => 'required',
            'ppn' => 'required',
            'pph' => 'required',
            'dpp' => 'required',
            'selling_price' => 'required',
            'min_stock' => 'required',
            'max_stock' => 'required',
            'store_id' => 'required'
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        $obj = [
            'product_code' => $request->product_code,
            'product_name' => $request->product_name,
            'unit_id' => $request->unit_id,
            'unit' => $request->unit,
            'category_id' => $request->category_id,
            'category' => $request->category,
            'merk_id' => $request->merk_id,
            'merk' => $request->merk,
            'description' => $request->descrtiption,
            'bkp' => $request->bkp,
            'purchase_price' => $request->purchase_price,
            'ppn' => $request->ppn,
            'pph' => $request->pph,
            'dpp' => $request->dpp,
            'selling_price' => $request->selling_price,
            'min_stock' => $request->min_stock,
            'max_stock' => $request->max_stock,
            'store_id' => $request->store_id,
            'user_created' => Auth::user()->username,
            'user_updated' => Auth::user()->username
        ];
        try {
            $data = $this->productRepository->store($obj);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success insert'
                ],
                'data' => $data
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'insert failed',
                    'log' => $e->getMessage()
                ]
            ];
        }

        return response($res, $res['metadata']['code']);
    }

    public function update($request, $id)
    {
        $rules = [
            'product_id' => 'required',
            'product_code' => 'required',
            'product_name' => 'required',
            'unit_id' => 'required',
            'unit' => 'required',
            'category_id' => 'required',
            'category' => 'required',
            'merk_id' => 'required',
            'merk' => 'required',
            'bkp' => 'required',
            'purchase_price' => 'required',
            'ppn' => 'required',
            'pph' => 'required',
            'dpp' => 'required',
            'selling_price' => 'required',
            'min_stock' => 'required',
            'max_stock' => 'required',
            'store_id' => 'required'
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        try {
            $update = $this->productRepository->update($request, $id);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success update'
                ],
                'data' => $update
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'failed update',
                    'log' => $e->getMessage()
                ]
            ];
        }
        return $res;
    }

    public function delete($id)
    {
        try {
            $delete = $this->productRepository->delete($id);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success delete'
                ],
                'data' => $delete
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'failed delete',
                    'log' => $e->getMessage()
                ]
            ];
        }
        return $res;
    }




}