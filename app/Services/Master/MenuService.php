<?php

namespace App\Services\Master;
use App\Repositories\Master\MenuRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use DataTables;

class MenuService
{

    protected $menuRepository;
    
    public function __construct(MenuRepository $menuRepository)
    {
        $this->menuRepository = $menuRepository;
    }

    public function allData($request, $status)
    {
        $data = $status === 1 ? $this->menuRepository->getAll():
        $this->menuRepository->getByStore($request);
        
        if($request->ajax()){
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<button type="button" id="btn-child" 
                    data-toggle="tooltip" data-placement="top" title="Add Child"
                    data-id="'.$row->id.'"
                    data-menu="'.$row->menu.'"
                    data-parent="'.$row->parent.'"
                    class="btn btn-circle btn-primary btn-sm"><i class="fas fa-sitemap"></i></button>
                    <button type="button" id="btn-edit" 
                    data-toggle="tooltip" data-placement="top" title="Edit"
                    data-id="'.$row->id.'"
                    data-menu="'.$row->menu.'"
                    data-url="'.$row->url.'"
                    data-icon="'.$row->icon.'"
                    data-parent="'.$row->parent.'"
                    class="btn btn-circle btn-success btn-sm"><i class="far fa-edit"></i></button>  
                    <button id="btn-delete" 
                    data-toggle="tooltip" data-placement="top" title="Delete"
                    data-id="'.$row->id.'"
                    class="btn btn-danger btn-circle btn-sm"><i class="fas fa-trash"></i></button>
                    ';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function getAll()
    {
        $data = $this->menuRepository->getAll();
        return response($data);
    }
    
    public function store($request)
    {
        $rules = [
            'menu' => 'required',
            'url_menu' => 'required',
            'store_id' => 'required'
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        $obj = [
            'store_id' => $request->store_id,
            'parent' => $request->parent,
            'menu' => $request->menu,
            'url' => $request->url_menu,
            'icon' => $request->icon,
            'user_created' => Auth::user()->username,
            'user_updated' => Auth::user()->username
        ];
        try {
            $data = $this->menuRepository->store($obj);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success insert'
                ],
                'data' => $data
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'insert failed',
                    'log' => $e->getMessage()
                ]
            ];
        }

        return response($res, $res['metadata']['code']);
    }

    public function update($request, $id)
    {
        $rules = [
            'menu' => 'required',
            'parent' => 'required',
            'url_menu' => 'required',
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        try {
            $update = $this->menuRepository->update($request, $id);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success update'
                ],
                'data' => $update
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'failed update',
                    'log' => $e->getMessage()
                ]
            ];
        }
        return $res;
    }

    public function delete($id)
    {
        try {
            $delete = $this->menuRepository->delete($id);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success delete'
                ],
                'data' => $delete
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'failed delete',
                    'log' => $e->getMessage()
                ]
            ];
        }
        return $res;
    }
}