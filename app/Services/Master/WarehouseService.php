<?php

namespace App\Services\Master;
use App\Repositories\Master\WarehouseRepository;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class WarehouseService
{
    protected $warehouseRepository;
    
    public function __construct(WarehouseRepository $warehouseRepository)
    {
        $this->warehouseRepository = $warehouseRepository;
    }
    
    public function allData($request, $status)
    {
        $data = $status === 'all' ? $this->warehouseRepository->getAll():
        $this->warehouseRepository->getByStore($request);

        if($request->ajax()){
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<button type="button" id="btn-edit" 
                    data-id="'.$row->id.'"
                    data-warehouse="'.$row->warehouse.'"
                    data-address="'.$row->address.'"
                    class="btn btn-circle btn-success btn-sm"><i class="far fa-edit"></i></button> 
                    <button id="btn-delete" 
                    data-id="'.$row->id.'"
                    class="btn btn-danger btn-circle btn-sm"><i class="fas fa-trash"></i></button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }
    
    public function store($request)
    {
        $rules = [
            'warehouse' => 'required',
            'address' => 'required',
            'store_id' => 'required'
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        $obj = [
            'warehouse' => $request->warehouse,
            'address' => $request->address,
            'store_id' => $request->store_id,
            'user_created' => Auth::user()->username,
            'user_updated' => Auth::user()->username
        ];
        try {
            $data = $this->warehouseRepository->store($obj);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success insert'
                ],
                'data' => $data
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'insert failed',
                    'log' => $e->getMessage()
                ]
            ];
        }

        return response($res);
    }

    public function update($request, $id)
    {
        $rules = [
            'warehouse' => 'required',
            'address' => 'required'
        ];

        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        try {
            $update = $this->warehouseRepository->update($request, $id);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success update'
                ],
                'data' => $update
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'failed update',
                    'log' => $e->getMessage()
                ]
            ];
        }
        return $res;
    }

    public function delete($id)
    {
        try {
            $delete = $this->warehouseRepository->delete($id);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success delete'
                ],
                'data' => $delete
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'failed delete',
                    'log' => $e->getMessage()
                ]
            ];
        }
        return $res;
    }

}