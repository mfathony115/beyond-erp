<?php

namespace App\Services\Master;
use App\Repositories\Master\MerkRepository;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class MerkService 
{
    protected $merkRepository;
    
    public function __construct(MerkRepository $merkRepository)
    {
       $this->merkRepository = $merkRepository;
    }

    public function allData($request, $status)
    {
        $data = $status === 'all' ? $this->merkRepository->getAll():
        $this->merkRepository->getByStore($request);

        if($request->ajax()){
            $data = $this->merkRepository->getAll($request, $status);
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<button type="button" id="btn-edit" 
                    data-id="'.$row->id.'"
                    data-merk_code="'.$row->merk_code.'"
                    data-merk="'.$row->merk.'"
                    class="btn btn-circle btn-success btn-sm"><i class="far fa-edit"></i></button> 
                    <button id="btn-delete" 
                    data-id="'.$row->id.'"
                    class="btn btn-danger btn-circle btn-sm"><i class="fas fa-trash"></i></button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function data()
    {
        $data = $this->merkRepository->getAll();
        return response($data);
    }
    
    public function store($request)
    {
        $rules = [
            'merk_code' => 'required',
            'merk' => 'required',
            'store_id' => 'required'
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        $obj = [
            'store_id' => $request->store_id,
            'merk_code' => $request->merk_code,
            'merk' => $request->merk,
            'user_created' => Auth::user()->username,
            'user_updated' => Auth::user()->username
        ];
        try {
            $data = $this->merkRepository->store($obj);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success insert'
                ],
                'data' => $data
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'insert failed',
                    'log' => $e->getMessage()
                ]
            ];
        }

        return response($res);
    }

    public function update($request, $id)
    {
        $rules = [
            'merk' => 'required'
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        try {
            $update = $this->merkRepository->update($request, $id);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success update'
                ],
                'data' => $update
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'failed update',
                    'log' => $e->getMessage()
                ]
            ];
        }
        return $res;
    }

    public function delete($id)
    {
        try {
            $delete = $this->merkRepository->delete($id);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success delete'
                ],
                'data' => $delete
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'failed delete',
                    'log' => $e->getMessage()
                ]
            ];
        }
        return $res;
    }
}