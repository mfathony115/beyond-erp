<?php

namespace App\Services\Master;

use App\Models\Master\RoleMenu;
use App\Repositories\Master\PositionRepository;
use App\Repositories\Master\MenuRepository;
use App\Repositories\Master\RoleMenuRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use DataTables;
use Batch;

class PositionService 
{
    protected $positionRepository;
    protected $menu;
    protected $roleMenu;
    
    public function __construct(PositionRepository $positionRepository)
    {
        $this->positionRepository = $positionRepository;
        $this->menu = new MenuRepository();
        $this->roleMenu = new RoleMenuRepository();
    }

    public function allData($request, $id)
    {
        $data = $id === 1 ? $this->positionRepository->getAll():
        $this->positionRepository->getByStore($id);
        if($data){
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<button type="button" id="btn-edit" 
                    data-id="'.$row->id.'"
                    data-position_code="'.$row->position_code.'"
                    data-position="'.$row->position.'"
                    class="btn btn-circle btn-success btn-sm"><i class="far fa-edit"></i></button> 
                    <button id="btn-delete" 
                    data-id="'.$row->id.'"
                    class="btn btn-danger btn-circle btn-sm"><i class="fas fa-trash"></i></button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    public function getByStore($id)
    {
        $data = $this->positionRepository->getByStore($id);
        $res = [
            'metadata' => [
                'code' => 200,
                'message' => 'success'
            ],
            'data' => $data
        ];
        return response($res, $res['metadata']['code']);
    }

    public function store($request)
    {
        $rules = [
            'position_code' => 'required',
            'position' => 'required',
            'store_id' => 'required',
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        $obj = [
            'store_id' => $request->store_id,
            'position' => $request->position,
            'position_code' => $request->position_code,
            'user_created' => Auth::user()->username,
            'user_updated' => Auth::user()->username
        ];
        try {
            $data = $this->positionRepository->store($obj);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success insert'
                ],
                'data' => $data
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'insert failed',
                    'log' => $e->getMessage()
                ]
            ];
        }

        return response($res);
    }
    
    public function update($request, $id)
    {
        $rules = [
            'position' => 'required'
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        try {
            $update = $this->positionRepository->update($request, $id);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success update'
                ],
                'data' => $update
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'failed update',
                    'log' => $e->getMessage()
                ]
            ];
        }
        return $res;
    }

    public function delete($id)
    {
        try {
            $delete = $this->positionRepository->delete($id);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success delete'
                ],
                'data' => $delete
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'failed delete',
                    'log' => $e->getMessage()
                ]
            ];
        }
        return $res;
    }

    public function getMasterMenu($id, $position_id)
    {
        $data = $this->menu->getByStore($id);
        $data_user = $this->roleMenu->getRoleMenuByPosition_($id, $position_id);
        
        $col_user = array_column($data_user->toArray(), 'menu_id');
        $tmp = [];
        foreach($data as $key => $value){
            $check = array_search($value->id, $col_user);
            if($value->parent === 0){
                $tmp[] = [
                    'id' => $value->id,
                    'menu' => $value->menu,
                    'url' => $value->url,
                    'icon' => $value->icon,
                    'status' => $check,
                    'child' => []
                ];      
            }
            $x = array_column($tmp, 'id');
            $xkey = array_search($value->parent, $x);
            if($value->parent === $data[$xkey]['id']){
                $tmp[$xkey]['child'][] = [
                    'id' => $value->id,
                    'menu' => $value->menu,
                    'url' => $value->url,
                    'icon' => $value->icon,
                    'status' => $check,
                ];
            }       
        }
        $res = [
            'metadata' => [
                'code' => 200,
                'message' => 'ok'
            ],
            'data' => $tmp
        ];
        return response($res, $res['metadata']['code']);
    }

    public function roleByPosition($request)
    {
        $rules = [
            'position_id' => 'required',
            'store_id' => 'required'
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        $data = $this->roleMenu->getRoleMenuByPosition_($request->store_id, $request->position_id);
        //return response($data);
        $tmp = [];
        foreach ($data as $key => $value) {
            if($value->parent === 0){
                $tmp[] = [
                    'id' => $value->id,
                    'menu_id' => $value->menu_id,
                    'menu' => $value->menu,
                    'parent' => $value->parent,
                    'child' => [],
                    'icon' => $value->icon,
                    'url' => $value->url,
                    'number' => $value->number,
                    'position_id' => $value->position_id,
                    'position' => $value->position,
                    'store_id' => $value->store_id
                ];
            }

            $x = array_column($tmp, 'menu_id');
            $xkey = array_search($value->parent, $x);
            if($value->parent === $data[$xkey]->menu_id){
                $tmp[$xkey]['child'][] = [
                    'id' => $value->id,
                    'menu_id' => $value->id,
                    'menu' => $value->menu,
                    'url' => $value->url,
                    'icon' => $value->icon,                    
                ];
            }     
            
        }
        if(count($data) > 0){
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success'
                ],
                'data' => $tmp
            ];
            return response($res, 200);
        }
        $res = [
            'metadata' => [
                'code' => 208,
                'message' => 'data tidak ditemukan'
            ],
            'data' => []
        ];
        return response($res, 208);
    }

    public function addRoleByPosition($request)
    {
        $rules = [
            'store_id' => 'required',
            'position_id' => 'required',
            'menu_id' => 'required',
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        //$getMenu = $this->menu->getById($menu_id);
        $obj = [
            'position_id' => $request->position_id,
            'menu_id' => $request->menu_id,
            'number' => 0,
            'store_id' => $request->store_id,
            'user_created' => Auth::user()->username,
            'user_updated' => Auth::user()->username
        ];
        $checkAvailable = $this->roleMenu->availableRole($request->position_id, $request->menu_id, $request->store_id);
        if(count($checkAvailable) > 0){
            $res = [
                'metadata' => [
                    'code' => 203,
                    'message' => 'menu is available'
                ]
            ];
            return response($res);
        }
        try {
            $insert = $this->roleMenu->addRoleByPosition($obj);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success'
                ], 'data' => $insert
            ];
            return response($res);
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'messsage' => $e->getMessage()
                ]
            ];
            return response($res, 500);
        }
    }

    public function deleteRoleByPosition($request)
    {
        $rules = [
            'store_id' => 'required',
            'position_id' => 'required',
            'menu_id' => 'required',
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        
        try {
            $delete = $this->roleMenu->deleteRoleByPosition($request->position_id, $request->menu_id, $request->store_id);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success'
                ]
            ];
            return response($res);
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'messsage' => $e->getMessage()
                ]
            ];
            return response($res, 500);
        }
    }

    public function getRoleMenuByPosition_($request)
    {
        $position_id = Auth::user()->position_id === null ? Auth::user()->position_id : $request->get('position_id');
        $store_id = Auth::user()->store_id === null ? Auth::user()->store_id: $request->get('store_id');
        $data = $this->roleMenu->getRoleMenuByPosition_($store_id, $position_id);
        //return response($data);die;
        $tmp = [];
        foreach ($data as $key => $value) {
            if($value->parent === 0){
                $tmp[] = [
                    'id' => $value->id,
                    'menu_id' => $value->menu_id,
                    'menu' => $value->menu,
                    'parent' => $value->parent,
                    'child' => [],
                    'icon' => $value->icon,
                    'url' => $value->url,
                    'number' => $value->number,
                    'position_id' => $value->position_id,
                    'position' => $value->position,
                    'store_id' => $value->store_id
                ];
            }

            $x = array_column($tmp, 'menu_id');
            // $tmp['test'] = $x;
            $xkey = array_search($value->parent, $x);
            // $tmp['tes1'][] = $xkey;
            if($value->parent === $data[$xkey]->menu_id){
                $tmp[$xkey]['child'][] = [
                    'id' => $value->id,
                    'menu_id' => $value->id,
                    'menu' => $value->menu,
                    'url' => $value->url,
                    'icon' => $value->icon,                    
                ];
            }     
            
        }
        if(count($data) > 0){
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success'
                ],
                'data' => $tmp
            ];
            return response($res, 200);
        }
        $res = [
            'metadata' => [
                'code' => 208,
                'message' => 'data tidak ditemukan'
            ],
            'data' => []
        ];
        return response($res, 208);
        //return response($data);
    }

    public function moveMenuByRole($request)
    {
        $obj = $request->sort;
        $no = 0;
        $field = [];
        $id_role = [];
        foreach ($obj as $key => $value) {
            $no++;
            $id_role[] = $value['id'];
            $field[] = [
                'id' => $value['id'],
                'number' => $no
            ];
        }
        try {
            $roleInstance = new RoleMenu;
            $update = Batch::update($roleInstance, $field, 'id');
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => $update
                ]
            ];
            return response($res, $res['metadata']['code']);
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'error',
                    'log' => $e->getMessage()
                ]
            ];
            return response($res, $res['metadata']['code']);
        }
    }
}