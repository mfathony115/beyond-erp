<?php

namespace App\Services\Master;

use App\Repositories\Master\CategoryRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use DataTables;

class CategoryService 
{
    protected $categoryRepository;
    
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function allData($request, $status)
    {
        $data = $status === 'all' ? $this->categoryRepository->getAll():
        $this->categoryRepository->getCategoryByStore($request);

        if($request->ajax()){
            $data = $this->categoryRepository->getAll($request, $status);
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<button type="button" id="btn-edit" 
                    data-id="'.$row->id.'"
                    data-category_code="'.$row->category_code.'"
                    data-category="'.$row->category.'"
                    class="btn btn-circle btn-success btn-sm"><i class="far fa-edit"></i></button> 
                    <button id="btn-delete" 
                    data-id="'.$row->id.'"
                    class="btn btn-danger btn-circle btn-sm"><i class="fas fa-trash"></i></button>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }
    
    public function getAll($store_id)
    {
        $data = $store_id === 1 ? 
        $this->categoryRepository->getAll():
        $this->categoryRepository->getCategoryByStore($store_id);
        $res = [
            'metadata' => [
                'code' => 200,
                'message' => 'success'
            ],
            'data' => $data
        ];

        return response($res, $res['metadata']['code']);
    }

    public function store($request)
    {
        $rules = [
            'category_code' => 'required',
            'category' => 'required',
            'store_id' => 'required'
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        $obj = [
            'store_id' => $request->store_id,
            'category' => $request->category,
            'category_code' => $request->category_code,
            'user_created' => Auth::user()->username,
            'user_updated' => Auth::user()->username
        ];
        try {
            $data = $this->categoryRepository->store($obj);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success insert'
                ],
                'data' => $data
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'insert failed',
                    'log' => $e->getMessage()
                ]
            ];
        }

        return response($res);
    }

    public function update($request, $id)
    {
        $rules = [
            'category' => 'required'
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        try {
            $update = $this->categoryRepository->update($request, $id);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success update'
                ],
                'data' => $update
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'failed update',
                    'log' => $e->getMessage()
                ]
            ];
        }
        return $res;
    }

    public function delete($id)
    {
        try {
            $delete = $this->categoryRepository->delete($id);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success delete'
                ],
                'data' => $delete
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'failed delete',
                    'log' => $e->getMessage()
                ]
            ];
        }
        return $res;
    }
}