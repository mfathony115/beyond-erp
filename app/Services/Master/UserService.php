<?php

namespace App\Services\Master;
use App\Repositories\Master\UserRepository;
use App\Repositories\Master\StoreRepository;
use Carbon\Carbon;
use DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserService
{
    protected $userRepository;
    
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getAllStore($id)
    {
        $store = new StoreRepository();
        $data = $id === 1 ? $store->getAll(): $store->getById($id);
        return $data;
    }

    public function getAll()
    {
        $data = $this->userRepository->getAll();
        $result = [];
        foreach ($data as $key => $value) {
            $result[] = [
                'id' => $value->id,
                'ktp' => $value->ktp,
                'username' => $value->username,
                'name' => $value->name,
                'birth' => $value->birth,
                'position' => $value->position,
                'email' => $value->email,
                'address' => $value->address,
                'action' => '-'
            ];
        }
        $res = [
            'metadata' => [
                'code' => 200,
                'message' => 'success'
            ],
            'data' => $result
        ];
        return response($res, $res['metadata']['code']);
    }

    public function allData($request, $id)
    {
        $data = $id === '1' ? $this->userRepository->getAll():
        $this->userRepository->getByStore($id);
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function($row){
                $actionBtn = '<button type="button" id="btn-edit" 
                data-id="'.$row->id.'"
                class="btn btn-circle btn-success btn-sm"><i class="far fa-edit"></i></button> 
                <button id="btn-delete" 
                data-id="'.$row->id.'"
                class="btn btn-danger btn-circle btn-sm"><i class="fas fa-trash"></i></button>';
                return $actionBtn;
            })
            ->rawColumns(['action'])
            ->make(true);
        
    }

    public function getByStore($id)
    {
        $data = $this->userRepository->getByStore($id);
        $res = [
            'metadata' => [
                'code' => 200,
                'message' => 'success'
            ],
            'data' => $data
        ];
        return response($res, $res['metadata']['code']);
    }

    public function getById($request, $id)
    {
        //$role = $request->get('role');
        $data = $this->userRepository->getById($id);
        $res = [
            'metadata' => [
                'code' => 200,
                'message' => 'success'
            ], 'data' => $data
        ];
        //$data->assignRole('admin');
        return response($res);
    }

    public function store($request)
    {
        $rules = [
            'ktp' => 'required:unique:users',
            'username' => 'required:unique:users',
            'email' => 'required:email:rfc,dns',
            'name' => 'required',
            'birth' => 'required',
            'store' => 'required',
            'store_id' => 'required',
            'position_id' => 'required',
            'position' => 'required',
            'address' => 'required',
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 202,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        $birth_ = Carbon::createFromFormat('d-m-Y', $request->birth)->format('Y-m-d');
        $obj = [
            'ktp' => $request->ktp,
            'username' => $request->username,
            'name' => $request->name,
            'birth' => $birth_,
            'store' => $request->store,
            'store_id' => $request->store_id,
            'position_id' => $request->position_id,
            'position' => $request->position,
            'email' => $request->email,
            'address' => $request->address,
            'password' => Hash::make($request->username),
            'user_created' => Auth::user()->username,
            'user_updated' => Auth::user()->username
        ];
        try {
            $data = $this->userRepository->store($obj);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success insert'
                ],
                'data' => $data
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'insert failed',
                    'log' => $e->getMessage()
                ]
            ];
        }

        return response($res, $res['metadata']['code']);
    }

    public function update($request, $id)
    {
        $rules = [
            'ktp' => 'required:unique:users',
            'username' => 'required:unique:users',
            'email' => 'required:email:rfc,dns',
            'name' => 'required',
            'birth' => 'required',
            'store' => 'required',
            'store_id' => 'required',
            'position_id' => 'required',
            'position' => 'required',
            'address' => 'required',
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 202,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        $birth_ = Carbon::createFromFormat('d-m-Y', $request->birth)->format('Y-m-d');
        $obj = [
            'ktp' => $request->ktp,
            'username' => $request->username,
            'name' => $request->name,
            'birth' => $birth_,
            'store' => $request->store,
            'store_id' => $request->store_id,
            'position_id' => $request->position_id,
            'position' => $request->position,
            'email' => $request->email,
            'address' => $request->address,
            'password' => Hash::make($request->username),
            'user_created' => Auth::user()->username,
            'user_updated' => Auth::user()->username
        ];
        try {
            $data = $this->userRepository->update($obj, $id);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success update'
                ],
                'data' => $data
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'update failed',
                    'log' => $e->getMessage()
                ]
            ];
        }

        return response($res, $res['metadata']['code']);
    }

    public function delete($id)
    {
        try {
            $delete = $this->userRepository->delete($id);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success delete'
                ],
                'data' => $delete
            ];
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'failed delete',
                    'log' => $e->getMessage()
                ]
            ];
        }
        return $res;
    }
    
}