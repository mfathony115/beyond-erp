<?php

namespace App\Services;

use App\Mail\Message;
use App\Repositories\RegisterRepository;
use App\Repositories\Master\StoreRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterService {

    protected $registerRepository;
    protected $storeRepository;
    public function __construct(RegisterRepository $registerRepository)
    {
        $this->registerRepository = $registerRepository;
        $this->storeRepository = new StoreRepository();
    }
    

    public function register($request)
    {
        $rules = [
            'fullname' => 'required',
            'username' => 'required|unique:users',
            'email' => 'required|email:rfc,dns|unique:users',
            'password' => 'required',
            'store_name' => 'required',
            'phone' => 'required',
            'address' => 'required'
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
        $objStore = [
            'npwp' => $request->npwp,
            'email' => $request->email,
            'store' => $request->store_name,
            'address' => $request->address,
            'status_data' => 2,
            'user_created' => 'reg',
            'user_updated' => 'reg'
        ];
        $store = $this->storeRepository->store($objStore);
        $position = $this->registerRepository->setDefaultPositions($store->id, $request->username);
        $obj = [
            'ktp' => date('Ymdhis'),
            'username' => $request->username,
            'name' => $request->fullname,
            'birth' => date('Y-m-d'),
            'store_id' => $store->id,
            'store' => $request->store_name,
            'position' => '-',
            'position_id' => $position->id,
            'email' => $request->email,
            'address' => $request->address,
            'status_data' => 2,
            'password' => Hash::make($request->password),
            'user_created' => 'reg',
            'user_updated' => 'reg'
        ];
        
        try {
            $new_user = $this->registerRepository->register($obj);
            $res = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success'
                ],
                'data' => $new_user
            ];
            Mail::to($request->email)->send(new Message($request));
            return response($res, $res['metadata']['code']);
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'server error',
                    'log' => $e->getMessage()
                ]
            ];
            return response($res, $res['metadata']['code']);
        }
    }

    public function addDefaultMenu($store_id, $username, $position_id)
    {
        $data = $this->registerRepository->getDefaultMenu();
        $no = 0;
        // $position = $this->registerRepository->setDefaultPositions($store_id, $username);
        foreach ($data as $key => $value) {
            $no++;
            $check = $this->registerRepository->checkAvailableMenu($store_id, $value->id);
            if(count($check) > 0){
                continue;
            }
            $obj = [
                'position_id' => $position_id,
                'menu_id' => $value->id,
                'number' => $no,
                'store_id' => $store_id,
                'status_data' => 0,
                'user_created' => $username,
                'user_updated' => $username
            ];
            $this->registerRepository->setDefaultMenu($obj);
        }
        $obj = [
            'position_id' => $position_id,
            'menu_id' => 1,
            'number' => 0,
            'store_id' => $store_id,
            'status_data' => 0,
            'user_created' => $username,
            'user_updated' => $username
        ];
        $this->registerRepository->setDefaultMenu($obj);
    }
}