<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Repositories\LoginRepository;
use App\Repositories\Master\UserRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Socialite;

class LoginService
{

    protected $loginRepository;

    public function __construct(LoginRepository $loginRepository)
    {
        $this->loginRepository = $loginRepository;
    }
    

    public function login($request)
    {
        $rules = [
            'email' => 'required:dns',
            'password' => 'required'
        ];
        $credentials = Validator::make($request->all(), $rules);
        if($credentials->fails()){
            $response = [
                'metadata' => [
                    'code' => 201,
                    'message' => $credentials->errors()
                ]
            ];
            return response($response, 201);
        }
         if (Auth::attempt($credentials->validated())) {
            $user = $this->loginRepository->login();
            if(Auth::user()->email_verified_at === null){
                $response = [
                    'metadata' => [
                        'code' => 203,
                        'message' => 'Please verification your email'
                    ]
                ];
                return response($response, 201);
            }
            $request->session()->regenerate();
            $access_token = $user->createToken('authToken')->accessToken;
            $response = [
                'metadata' => [
                    'code' => 200,
                    'message' => 'success'
                ], 
                'data' => Auth::user(),
                'token' => $access_token
            ];
            return response($response, 200);
        }
        
        $response = [
            'metadata' => [
                'code' => 201,
                'message' => 'Username or password incorrect'
            ]
        ];
        return response($response, 201);
    }

    public function redirectToGoogle(){
        return Socialite::driver('google')
            ->stateless()
            ->redirect();
    }

    public function oAuthGoogle()
    {
        try {
            $user = Socialite::driver('google')->stateless()->user();
        } catch (\Exception $e) {
            $res = [
                'metadata' => [
                    'code' => 500,
                    'message' => 'error',
                    'log' => $e->getMessage()
                ]
            ];
            return response($res, $res['metadata']['code']);
        }       
        $existingUser = User::where('email', $user->email)->first();        
        if($existingUser){
            auth()->login($existingUser, true);
            return redirect()->to('/');
        } else {
            return redirect('/login')->with('loginError', 'Unknown user, please create account !');
        }
    }

    public function redirectToProvider()
    {
        return Socialite::driver('google')->stateless()->redirect();
    }
}