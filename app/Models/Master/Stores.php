<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stores extends Model
{
    use HasFactory;

    protected $table = 'stores';
    protected $fillable = [
        'npwp',
        'email',
        'store',
        'address',
        'status_data',
        'user_created',
        'user_updated'
    ];

    protected $hidden = [
        'status_data',
        'user_created',
        'user_updated',
        'updated_at',
        'created_at'
    ];
}
