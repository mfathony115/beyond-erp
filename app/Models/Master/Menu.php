<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    protected $table = 'Menu';
    protected $fillable = [
        'menu',
        'number',
        'url',
        'parent',
        'icon',
        'store_id',
        'status_data',
        'user_created',
        'user_updated'
    ];

    protected $hidden = [
        'status_data',
        'user_created',
        'user_updated',
        'updated_at',
        'created_at'
    ];
}
