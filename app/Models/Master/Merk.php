<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Merk extends Model
{
    use HasFactory;
    protected $table = 'merks';
    protected $fillable = [
        'store_id',
        'merk_code',
        'merk',
        'photo',
        'status_data',
        'user_created',
        'user_updated'
    ];

    protected $hidden = [
        'status_data',
        'user_created',
        'user_updated',
        'updated_at',
        'created_at'
    ];
}
