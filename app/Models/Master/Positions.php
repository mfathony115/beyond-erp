<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Positions extends Model
{
    use HasFactory;

    protected $table = 'positions';
    protected $fillable = [
        'store_id',
        'position_code',
        'position',
        'status_data',
        'user_created',
        'user_updated'
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
