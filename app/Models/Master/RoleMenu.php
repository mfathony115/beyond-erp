<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoleMenu extends Model
{
    use HasFactory;

    protected $table = 'role_menu';
    protected $fillable = [
        'position_id',
        'menu_id',
        'number',
        'store_id',
        'status_data',
        'user_created',
        'user_updated'
    ];
    public function menu()
    {
        return $this->belongsTo(Menu::class)->orderBy('parent', 'asc');
    }

    public function position()
    {
        return $this->belongsTo(Positions::class);
    }

    protected $hidden = [
        'status_data',
        'user_created',
        'user_updated',
        'updated_at',
        'created_at'
    ];
}
