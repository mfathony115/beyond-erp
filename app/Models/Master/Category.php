<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';
    protected $fillable = [
        'category_code',
        'category',
        'store_id',
        'status_data',
        'user_created',
        'user_updated'
    ];

    protected $hidden = [
        'status_data',
        'user_created',
        'user_updated',
        'updated_at',
        'created_at'
    ];
}
