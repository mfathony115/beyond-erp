<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $primaryKey = 'id';
    protected $fillable = [
        'product_code',
        'product_name',
        'unit_id',
        'unit',
        'category_id',
        'category',
        'merk_id',
        'merk',
        'description',
        'bkp',
        'purchase_price',
        'ppn',
        'pph',
        'dpp',
        'selling_price',
        'min_stock',
        'max_stock',
        'product_photo',
        'store_id',
        'user_created',
        'user_updated'
    ];

    public function merk()
    {
        return $this->belongsTo(Merk::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    public function store()
    {
        return $this->belongsTo(Stores::class);
    }

    protected $hidden = [
        'status_data',
        'user_created',
        'user_updated',
        'updated_at',
        'created_at'
    ];
}
