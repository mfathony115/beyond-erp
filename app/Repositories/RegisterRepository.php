<?php

namespace app\Repositories;

use App\Models\Master\Menu;
use App\Models\Master\Positions;
use App\Models\Master\RoleMenu;
use App\Models\Master\Stores;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class RegisterRepository
{
    public function register($obj)
    {
        try {
            DB::beginTransaction();
            $user = User::create($obj);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw $e;
        }
       
        return $user;
    }

    public function checkAvailableMenu($store_id, $menu_id)
    {
        $check = RoleMenu::where('position_id', 2)
            ->where('store_id', $store_id)
            ->where('menu_id', $menu_id)
            ->get();
        return $check;
    }

    public function getDefaultMenu()
    {
        $data = Menu::where('parent', 1)->get();
        return $data;
    }

    public function setDefaultMenu($obj)
    {
        $data = RoleMenu::create($obj);
        return $data;
    }

    public function setDefaultPositions($store_id, $username)
    {
        $obj = [
            'store_id' => $store_id,
            'position_code' => 'adm',
            'position' => 'position',
            'status_data' => 0,
            'user_created' => $username,
            'user_updated'=> $username
        ];
        $data = Positions::create($obj);
        return $data;
    }
}