<?php

namespace App\Repositories;

use Illuminate\Support\Facades\Auth;

class LoginRepository
{
    public function login()
    {
        $user = Auth::user();
        return $user;
    }
}