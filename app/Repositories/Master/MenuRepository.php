<?php

namespace App\Repositories\Master;

use App\Models\Master\Menu;
use Illuminate\Support\Facades\Auth;

class MenuRepository
{
    public function getAll()
    {
        $data = Menu::where('status_data', 0)->where('parent', 0)->get();
        return $data;
    }

    public function getById($id)
    {
        $data = Menu::findOrFail($id);
        return $data;
    }

    public function getByStore()
    {
        $data = Menu::where('status_data', 0)->get();
        return $data;
    }

    public function store($obj)
    {
        $data = Menu::create($obj);
        return $data;
    }

    public function update($request, $id)
    {
        $update = Menu::findOrFail($id);
        $update->menu = $request->menu;
        $update->parent = $request->parent;
        $update->url = $request->url_menu;
        $update->icon = $request->icon;
        $update->user_updated = Auth::user()->username;
        $update->updated_at = date('Y-m-d H:i:s');
        $update->save();
        return $update;
    }

    public function delete($id)
    {
        $delete = Menu::findOrFail($id);
        $delete->status_data = 1;
        $delete->user_updated = Auth::user()->username;
        $delete->updated_at = date('Y-m-d H:i:s');
        $delete->save();
        return $delete;
    }
}