<?php

namespace App\Repositories\Master;

use App\Models\Master\Stores;

class StoreRepository {

    public function getAll()
    {
        $data = Stores::all();
        return $data;
    }

    public function getById($id)
    {
        $data = Stores::where('id', $id)->get();
        return $data;
    }

    public function store($obj)
    {
        $data = Stores::create($obj);
        return $data;
    }
}