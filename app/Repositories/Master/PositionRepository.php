<?php

namespace App\Repositories\Master;

use App\Models\Master\Positions;
use Illuminate\Support\Facades\Auth;

class PositionRepository
{
    public function getAll()
    {
        $data = Positions::where('status_data', 0)->get();
        return $data;
    }

    public function getByStore($status)
    {
        $data = Positions::where('store_id', $status)->where('status_data', 0)->get();
        return $data;
    }

    public function store($request)
    {
        $data = Positions::create($request);
        return $data;
    }

    public function update($request, $id)
    {
        $update = Positions::findOrFail($id);
        $update->position = $request->position;
        $update->user_updated = Auth::user()->username;
        $update->updated_at = date('Y-m-d H:i:s');
        $update->save();
        return $update;
    }

    public function delete($id)
    {
        $delete = Positions::findOrFail($id);
        $delete->status_data = 1;
        $delete->user_updated = Auth::user()->username;
        $delete->updated_at = date('Y-m-d H:i:s');
        $delete->save();
        return $delete;
    }
}