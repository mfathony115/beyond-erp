<?php

namespace App\Repositories\Master;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserRepository
{
    public function getAll()
    {
        $data = User::where('status_data', 0)->get();
        return $data;
    }

    public function getByStore($store)
    {
        $data = User::where('store_id', $store)->where('status_data', 0)->get();
        return $data;
    }

    public function getById($id)
    {
        $data = User::findOrFail($id);
        return $data;
    }

    public function store($request)
    {
        $insert = User::create($request);
        return $insert;
    }

    public function update($obj, $id)
    {
        $update = User::where('id', $id)->update($obj);
        return $update;
    }

    public function delete($id)
    {
        $delete = User::findOrFail($id);
        $delete->status_data = 1;
        $delete->user_updated = Auth::user()->username;
        $delete->updated_at = date('Y-m-d H:i:s');
        $delete->save();
        return $delete;
    }
}