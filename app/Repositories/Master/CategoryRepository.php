<?php

namespace App\Repositories\Master;

use App\Models\Master\Category;
use Illuminate\Support\Facades\Auth;

class CategoryRepository
{

    public function getAll()
    {
        $data = Category::where('status_data', 0)->get();
        return $data;
    }

    public function getCategoryByStore($store_id)
    {
        $data = Category::where('store_id', $store_id)->firstOrFail();
        return $data;
    }

    public function store($obj)
    {
        $data = Category::create($obj);
        return $data;
    }

    public function update($request, $id)
    {
        $update = Category::findOrFail($id);
        $update->category = $request->category;
        $update->user_updated = Auth::user()->username;
        $update->updated_at = date('Y-m-d H:i:s');
        $update->save();
        return $update;
    }

    public function delete($id)
    {
        $delete = Category::findOrFail($id);
        $delete->status_data = 1;
        $delete->user_updated = Auth::user()->username;
        $delete->updated_at = date('Y-m-d H:i:s');

        $delete->save();
        return $delete;
    }
}