<?php

namespace App\Repositories\Master;

use App\Models\Master\RoleMenu;
use Batch;
use Illuminate\Support\Facades\DB;

class RoleMenuRepository
{
    public function getParent($store_id, $position_id)
    {
        $data = RoleMenu::with(['menu' => function($query){
            $query->orderBy('parent', 'asc');
        }])->where('store_id', $store_id)
            ->where('position_id', $position_id)->orderBy('number', 'asc')->get();
        return $data;
    }

    public function getRoleMenuByPosition_($store_id, $position_id)
    {
        $sql = DB::table('role_menu')
            ->join('menu', 'menu.id', '=', 'role_menu.menu_id')
            ->join('positions', 'positions.id', '=', 'role_menu.position_id')
            ->select('role_menu.id', 'role_menu.position_id', 'positions.position', 'role_menu.menu_id', 'menu.menu', 'menu.parent', 'menu.url', 'menu.icon', 'role_menu.number', 'role_menu.store_id')
            ->where('role_menu.store_id', $store_id)
            ->where('role_menu.position_id', $position_id);
        // if($store_id !== 1 ){
        //     $sql->where('role_menu.menu_id', 15);
        // }
         $data =  $sql->orderBy('menu.parent', 'asc')
            ->orderBy('role_menu.number', 'asc')
            ->get();
        return $data;
    }

    public function addRoleByPosition($obj)
    {
        $add = RoleMenu::create($obj);
        return $add;
    }

    public function deleteRoleByPosition($position_id, $menu_id, $store_id)
    {
        $delete = RoleMenu::where('position_id', $position_id)->where('store_id', $store_id)->where('menu_id', $menu_id)->delete();
        return $delete;
    }

    public function availableRole($position_id, $menu_id, $store_id)
    {
        $data = RoleMenu::where('position_id', $position_id)->where('store_id', $store_id)->where('menu_id', $menu_id)->get();
        return $data;
    }

    public function updateNumberPosition($id, $field)
    {
        $rolemenuInstance = new RoleMenu;
        $index = 'id';
        $data = Batch::update($rolemenuInstance, $field, $index);
        return $data;
    }
}