<?php

namespace App\Repositories\Master;

use App\Models\Master\Unit;
use Illuminate\Support\Facades\Auth;

class UnitRepository
{
    public function getAll()
    {
        $data = Unit::where('status_data', 0)->get();
        return $data;
    }

    public function getByStore($store_id)
    {
        $data = Unit::where('store_id', $store_id)->firstOrFail();
        return $data;
    }

    public function store($obj)
    {
        $data = Unit::create($obj);
        return $data;
    }

    public function update($request, $id)
    {
        $update = Unit::findOrFail($id);
        $update->unit = $request->unit;
        $update->user_updated = Auth::user()->username;
        $update->updated_at = date('Y-m-d H:i:s');
        $update->save();
        return $update;
    }

    public function delete($id)
    {
        $delete = Unit::findOrFail($id);
        $delete->status_data = 1;
        $delete->user_updated = Auth::user()->username;
        $delete->updated_at = date('Y-m-d H:i:s');
        $delete->save();
        return $delete;
    }
}