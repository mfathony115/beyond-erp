<?php

namespace App\Repositories\Master;

use App\Models\Master\Merk;
use Illuminate\Support\Facades\Auth;

class MerkRepository
{
    public function getAll()
    {
        $data = Merk::where('status_data', 0)->get();
        return $data;
    }

    public function getByStore($store_id)
    {
        $data = Merk::where('store_id', $store_id)->firstOrFail();
        return $data;
    }

    public function store($obj)
    {
        $data = Merk::create($obj);
        return $data;
    }

    public function update($request, $id)
    {
        $update = Merk::findOrFail($id);
        $update->merk = $request->merk;
        $update->user_updated = Auth::user()->username;
        $update->updated_at = date('Y-m-d H:i:s');
        $update->save();
        return $update;
    }

    public function delete($id)
    {
        $delete = Merk::findOrFail($id);
        $delete->status_data = 1;
        $delete->user_updated = Auth::user()->username;
        $delete->updated_at = date('Y-m-d H:i:s');
        $delete->save();
        return $delete;
    }
}