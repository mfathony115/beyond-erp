<?php

namespace App\Repositories\Master;

use App\Models\Master\Product;
use Illuminate\Support\Facades\Auth;

class ProductRepository
{

    public function getAll()
    {
        $data = Product::where('status_data', 0)->get();
        return $data;
    }

    public function getByStore($store_id)
    {
        $data = Product::where('store_id', $store_id)->get();
        return $data;
    }

    public function getById($id)
    {
        $data = Product::with(['merk', 'unit', 'category', 'store'])->findOrFail($id);
        return $data;
    }

    public function store($obj)
    {
        $data = Product::create($obj);
        return $data;
    }

    public function update($request, $id)
    {
        $update = Product::findOrFail($id);
        $update->product_code = $request->product_code;
        $update->product_name = $request->product_name;
        $update->unit_id = $request->unit_id;
        $update->unit = $request->unit;
        $update->category_id = $request->category_id;
        $update->category = $request->category;
        $update->merk_id = $request->merk_id;
        $update->merk = $request->merk;
        $update->description = $request->description;
        $update->bkp = $request->bkp;
        $update->purchase_price = $request->purchase_price;
        $update->ppn = $request->ppn;
        $update->pph = $request->pph;
        $update->dpp = $request->dpp;
        $update->selling_price = $request->selling_price;
        $update->min_stock = $request->min_stock;
        $update->max_stock = $request->max_stock;
        $update->product_photo = $request->product_photo;
        $update->user_updated = Auth::user()->username;
        $update->updated_at = date('Y-m-d H:i:s');
        $update->save();
        return $update;
    }

    public function delete($id)
    {
        $delete = Product::findOrFail($id);
        $delete->status_data = 1;
        $delete->user_updated = Auth::user()->username;
        $delete->updated_at = date('Y-m-d H:i:s');
        $delete->save();
        return $delete;
    }
}