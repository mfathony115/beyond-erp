<?php

namespace App\Repositories\Master;

use App\Models\Master\Vendor;
use Illuminate\Support\Facades\Auth;

class VendorRepository
{
    public function getAll()
    {
        $data = Vendor::where('status_data', 0)->get();
        return $data;
    }

    public function searchData($name)
    {
        $data = Vendor::where('name', 'like', "%$name%")->get();
        return $data;
    }

    public function getByStore($store_id)
    {
        $data = Vendor::where('store_id', $store_id)->firstOrFail();
        return $data;
    }

    public function store($obj)
    {
        $data = Vendor::create($obj);
        return $data;
    }

    public function update($request, $id)
    {
        $update = Vendor::findOrFail($id);
        $update->name = $request->name;
        $update->email = $request->email;
        $update->telp = $request->telp;
        $update->address = $request->address;
        $update->npwp = $request->npwp;
        $update->tipe = $request->tipe === 'perusahaan' ? 0 : 1;
        $update->user_updated = Auth::user()->username;
        $update->updated_at = date('Y-m-d H:i:s');
        $update->save();
        return $update;
    }

    public function delete($id)
    {
        $delete = Vendor::findOrFail($id);
        $delete->status_data = 1;
        $delete->user_updated = Auth::user()->username;
        $delete->updated_at = date('Y-m-d H:i:s');
        $delete->save();
        return $delete;
    }
}