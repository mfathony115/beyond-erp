<?php

namespace App\Repositories\Master;
use App\Models\Master\Warehouse;
use Illuminate\Support\Facades\Auth;

class WarehouseRepository
{

    public function getAll()
    {
        $data = Warehouse::where('status_data', 0)->get();
        return $data;
    }

    public function getByStore($store_id)
    {
        $data = Warehouse::where('store_id', $store_id)->firstOrFail();
        return $data;
    }

    public function store($obj)
    {
        $data = Warehouse::create($obj);
        return $data;
    }

    public function update($request, $id)
    {
        $update = Warehouse::findOrFail($id);
        $update->warehouse = $request->warehouse;
        $update->address = $request->address;
        $update->user_updated = Auth::user()->username;
        $update->updated_at = date('Y-m-d H:i:s');
        $update->save();
        return $update;
    }

    public function delete($id)
    {
        $delete = Warehouse::findOrFail($id);
        $delete->status_data = 1;
        $delete->user_updated = Auth::user()->username;
        $delete->updated_at = date('Y-m-d H:i:s');
        $delete->save();
        return $delete;
    }
}